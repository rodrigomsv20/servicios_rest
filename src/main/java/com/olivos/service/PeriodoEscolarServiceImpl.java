package com.olivos.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.olivos.model.PeriodoEscolar;
import com.olivos.repository.PeriodoEscolarRepository;

@Service
public class PeriodoEscolarServiceImpl implements PeriodoEscolarService
{
	
	@Autowired
	private PeriodoEscolarRepository repository;
	
	@Override
	@Transactional
	public void insert(PeriodoEscolar periodoEscolar) {
		repository.save(periodoEscolar);
	}

	@Override
	@Transactional
	public void update(PeriodoEscolar periodoEscolar) {
		repository.save(periodoEscolar);
	}

	@Override
	@Transactional
	public void delete(Integer periodoescolar_id) {
		repository.deleteById(periodoescolar_id);
	}

	@Override
	@Transactional(readOnly=true)
	public PeriodoEscolar findById(Integer periodoescolar_id) {
		return repository.findById(periodoescolar_id).orElse(null);
	}

	@Override
	@Transactional(readOnly=true)
	public Collection<PeriodoEscolar> findAll() {
		return (Collection<PeriodoEscolar>)repository.findAll();
	}

}
