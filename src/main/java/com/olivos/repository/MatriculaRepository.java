package com.olivos.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.olivos.model.Matricula;

public interface MatriculaRepository extends CrudRepository<Matricula, Integer>{
	
	@Query(value = "{call usp_getListarAlumGrado(:grado_id)}", nativeQuery = true)
	public abstract Collection<Matricula> listaAlumnoGrado(@Param("grado_id") Integer grado_id);
	
	@Query(value = "{call usp_getGradoAlum(:alumno_id)}", nativeQuery = true)
	public abstract Matricula getGradoAlum(@Param("alumno_id") Integer grado_id);
	
}
