package com.olivos.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.olivos.model.Curso;

public interface CursoRepository extends CrudRepository<Curso, Integer>{

	@Query(value = "{call usp_getListarCursoGrado(:grado_id)}", nativeQuery = true)
	public abstract Collection<Curso> listarCursoGrado(@Param("grado_id") Integer grado_id);
}
