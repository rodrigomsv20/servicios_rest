package com.olivos.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.olivos.model.Calificacion;

public interface CalificacionRepository extends CrudRepository<Calificacion, Integer>{
	
	@Query(value = "{call usp_getListaCalifAlums(:grado_id,:curso_id,:periodoescolar_id)}", nativeQuery = true)
	public abstract Collection<Calificacion> listaCalifAlums(@Param("grado_id") Integer grado_id, @Param("curso_id") Integer curso_id, @Param("periodoescolar_id") Integer periodoescolar_id);
	
	@Query(value = "{call usp_getCalifAlumno(:alumno_id,:curso_id,:periodoescolar_id)}", nativeQuery = true)
	public abstract Calificacion califAlumno(@Param("alumno_id")Integer alumno_id,@Param("curso_id")Integer curso_id,@Param("periodoescolar_id")Integer periodoescolar_id);
	
	@Query(value = "{call usp_getCalifAlumnoPerEscolar(:alumno_id,:periodoescolar_id)}", nativeQuery = true)
	public abstract Collection<Calificacion> califAlumnoPerEscolar(@Param("alumno_id") Integer alumno_id, @Param("periodoescolar_id") Integer periodoescolar_id);
	
}
