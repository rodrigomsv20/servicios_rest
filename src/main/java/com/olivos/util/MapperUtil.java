package com.olivos.util;

import java.util.ArrayList;
import java.util.Collection;

import com.olivos.mapper.AlumnoMapper;
import com.olivos.mapper.AsistenciaMapper;
import com.olivos.mapper.CalificacionMapper;
import com.olivos.mapper.CitacionMapper;
import com.olivos.mapper.CursoMapper;
import com.olivos.mapper.EmpleadoMapper;
import com.olivos.mapper.GradoMapper;
import com.olivos.mapper.IncidenciaMapper;
import com.olivos.mapper.MatriculaMapper;
import com.olivos.mapper.PerfilMapper;
import com.olivos.mapper.PeriodoEscolarMapper;
import com.olivos.mapper.TareaMapper;
import com.olivos.mapper.UsuarioAlumnoMapper;
import com.olivos.mapper.UsuarioEmpleadoMapper;
import com.olivos.mapper.VerAsistenciaMapper;
import com.olivos.mapper.VerCalificacionMapper;
import com.olivos.mapper.VerCitacionMapper;
import com.olivos.mapper.VerIncidenciaMapper;
import com.olivos.mapper.VerTareaMapper;
import com.olivos.model.Alumno;
import com.olivos.model.Asistencia;
import com.olivos.model.Calificacion;
import com.olivos.model.Citacion;
import com.olivos.model.Curso;
import com.olivos.model.Empleado;
import com.olivos.model.Grado;
import com.olivos.model.Incidencia;
import com.olivos.model.Matricula;
import com.olivos.model.Perfil;
import com.olivos.model.PeriodoEscolar;
import com.olivos.model.Tarea;
import com.olivos.model.UsuarioAlumno;
import com.olivos.model.UsuarioEmpleado;

public class MapperUtil 
{
	public static Collection<PerfilMapper> convertPerfMapper(Collection<Perfil> itemsPerfil)
	{
		Collection<PerfilMapper> collection = new ArrayList<>();
		
		for (Perfil perfil : itemsPerfil)
		{
			collection.add(new PerfilMapper(perfil));
		}
		return collection;
	}
	
	public static Collection<UsuarioEmpleadoMapper> convertUsuaEmpMapper(Collection<UsuarioEmpleado> itemsUsuario)
	{
		Collection<UsuarioEmpleadoMapper> collection = new ArrayList<>();
		
		for(UsuarioEmpleado usuario : itemsUsuario)
		{
			collection.add(new UsuarioEmpleadoMapper(usuario));
		}
		return collection;
	}
	
	public static Collection<UsuarioAlumnoMapper> convertUsuaAlumMapper(Collection<UsuarioAlumno> itemsUsuario)
	{
		Collection<UsuarioAlumnoMapper> collection = new ArrayList<>();
		
		for(UsuarioAlumno usuario : itemsUsuario)
		{
			collection.add(new UsuarioAlumnoMapper(usuario));
		}
		return collection;
	}
	
	public static Collection<AlumnoMapper> convertAlumMapper(Collection<Alumno> itemsAlumno)
	{
		Collection<AlumnoMapper> collection = new ArrayList<>();
		
		for(Alumno alumno : itemsAlumno)
		{
			collection.add(new AlumnoMapper(alumno));
		}
		return collection;
	}
	
	public static Collection<GradoMapper> convertGradoMapper(Collection<Grado> itemsGrado)
	{
		Collection<GradoMapper> collection = new ArrayList<>();
		
		for(Grado grado : itemsGrado)
		{
			collection.add(new GradoMapper(grado));
		}
		return collection;
	}
	
	public static Collection<MatriculaMapper> convertMatriculaMapper(Collection<Matricula> itemsMatricula)
	{
		Collection<MatriculaMapper> collection = new ArrayList<>();
		
		for(Matricula matricula : itemsMatricula)
		{
			collection.add(new MatriculaMapper(matricula));
		}
		return collection;
	}
	
	public static Collection<CursoMapper> convertCursoMapper(Collection<Curso> itemsCurso)
	{
		Collection<CursoMapper> collection = new ArrayList<>();
		
		for(Curso curso : itemsCurso)
		{
			collection.add(new CursoMapper(curso));
		}
		return collection;
	}
	
	public static Collection<AsistenciaMapper> convertAsistMapper(Collection<Asistencia> itemsAsistencia)
	{
		Collection<AsistenciaMapper> collection = new ArrayList<>();
		
		for(Asistencia asistencia : itemsAsistencia)
		{
			collection.add(new AsistenciaMapper(asistencia));
		}
		return collection;
	}
	
	//Mapper Autentificación
	public static AlumnoMapper convertAlumnoMapper(Alumno alumno) {
		AlumnoMapper alumnoMapper= new AlumnoMapper(alumno);
		return alumnoMapper;
		
	}
	
	public static EmpleadoMapper convertEmpleadoMapper(Empleado empleado) {
		EmpleadoMapper empleadoMapper = new EmpleadoMapper(empleado);
		return empleadoMapper;
	}
	
	//Mapper Asistencias
	public static TareaMapper convertTareaMapper(Tarea tarea) {
		TareaMapper tareaMapper = new TareaMapper(tarea);
		return tareaMapper;
	}
	
	public static Collection<TareaMapper> convertTareasMapper(Collection<Tarea> itemsTarea) {

		Collection<TareaMapper> collection = new ArrayList<>();
		
		for(Tarea tarea : itemsTarea)
		{
			collection.add(new TareaMapper(tarea));
		}
		return collection;
	}
	
	//Mapper Incidencias
	public static Collection<IncidenciaMapper> convertIncidenciaMapper(Collection<Incidencia> itemsIncidencia) {
		
		Collection<IncidenciaMapper> collection = new ArrayList<>();
		
		for(Incidencia incidencia : itemsIncidencia)
		{
			collection.add(new IncidenciaMapper(incidencia));
		}
		return collection;
	}
	
	public static Collection<CitacionMapper> convertCitacionMapper(Collection<Citacion> itemsCitacion) {
		
		Collection<CitacionMapper> collection = new ArrayList<>();
		
		for(Citacion citacion : itemsCitacion)
		{
			collection.add(new CitacionMapper(citacion));
		}
		return collection;
	}
	
	public static Collection<PeriodoEscolarMapper> convertPeriodoEscolarMapper(Collection<PeriodoEscolar> itemsPeriodoEscolar) {
		
		Collection<PeriodoEscolarMapper> collection = new ArrayList<>();
		
		for(PeriodoEscolar periodoEscolar : itemsPeriodoEscolar)
		{
			collection.add(new PeriodoEscolarMapper(periodoEscolar));
		}
		return collection;
	}
	
	public static Collection<CalificacionMapper> convertCalificacionMapper(Collection<Calificacion> itemsCalificacion) {
		
		Collection<CalificacionMapper> collection = new ArrayList<>();
		
		for(Calificacion calificacion : itemsCalificacion)
		{
			collection.add(new CalificacionMapper(calificacion));
		}
		return collection;
	}
	
	public static Collection<VerTareaMapper> convertVerTareaMapper(Collection<Tarea> itemsTarea) {
		
		Collection<VerTareaMapper> collection = new ArrayList<>();
		
		for(Tarea tarea : itemsTarea)
		{
			collection.add(new VerTareaMapper(tarea));
		}
		return collection;
	}
	
	public static Collection<VerIncidenciaMapper> convertVerIncidenciaMapper(Collection<Incidencia> itemsIncidencia) {
		
		Collection<VerIncidenciaMapper> collection = new ArrayList<>();
		
		for(Incidencia incidencia : itemsIncidencia)
		{
			collection.add(new VerIncidenciaMapper(incidencia));
		}
		return collection;
	}
	
	public static Collection<VerCitacionMapper> convertVerCitacionMapper(Collection<Citacion> itemsCitacion) {
		
		Collection<VerCitacionMapper> collection = new ArrayList<>();
		
		for(Citacion citacion : itemsCitacion)
		{
			collection.add(new VerCitacionMapper(citacion));
		}
		return collection;
	}
	
	public static Collection<VerCalificacionMapper> convertVerCalificacionMapper(Collection<Calificacion> itemsCalificacion) {
		
		Collection<VerCalificacionMapper> collection = new ArrayList<>();
		
		for(Calificacion calificacion : itemsCalificacion)
		{
			collection.add(new VerCalificacionMapper(calificacion));
		}
		return collection;
	}
	
	public static Collection<VerAsistenciaMapper> convertVerAsistenciaMapper(Collection<Asistencia> itemsAsistencia) {
		
		Collection<VerAsistenciaMapper> collection = new ArrayList<>();
		
		for(Asistencia asistencia : itemsAsistencia)
		{
			collection.add(new VerAsistenciaMapper(asistencia));
		}
		return collection;
	}
}

