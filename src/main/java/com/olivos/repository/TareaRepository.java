package com.olivos.repository;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.olivos.model.Tarea;

public interface TareaRepository extends CrudRepository<Tarea, Integer>{
	@Query(value = "{call usp_getTarea(:grado_id,:curso_id,:fecha_registro)}", nativeQuery = true)
	public abstract Tarea tarea(@Param("grado_id") Integer grado_id, 
			@Param("curso_id") Integer curso_id, @Param("fecha_registro") Date fecha_registro);
	
	@Query(value = "{call usp_getTareaAlum(:empleado_id,:curso_id,:grado_id,:descripcion,:fecha_registro)}", nativeQuery = true)
	public abstract Tarea tareaAlum(@Param("empleado_id")Integer empleado_id,@Param("curso_id")Integer curso_id,
			@Param("grado_id")Integer grado_id,@Param("descripcion") String descripcion,@Param("fecha_registro") String fecha_registro);
	
	@Query(value = "{call usp_getTareasGradoFecha(:alumno_id,:fecha_registro)}", nativeQuery = true)
	public abstract Collection<Tarea> tareasGradoFecha(@Param("alumno_id")Integer alumno_id,@Param("fecha_registro") Date fecha_registro);
	
}
