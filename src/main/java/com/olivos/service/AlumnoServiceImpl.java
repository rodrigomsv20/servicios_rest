package com.olivos.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.olivos.model.Alumno;
import com.olivos.repository.AlumnoRepository;

@Service
public class AlumnoServiceImpl implements AlumnoService
{
	@Autowired
	private AlumnoRepository repository;
	
	@Override
	@Transactional
	public void insert(Alumno alumno) {
		repository.save(alumno);
	}

	@Override
	@Transactional
	public void update(Alumno alumno) {
		repository.save(alumno);
	}

	@Override
	@Transactional
	public void delete(Integer alumno_id) {
		repository.deleteById(alumno_id);
	}

	@Override
	@Transactional(readOnly=true)
	public Alumno findById(Integer alumno_id) {
		return repository.findById(alumno_id).orElse(null);
	}

	@Override
	@Transactional(readOnly=true)
	public Collection<Alumno> findAll() {
		return (Collection<Alumno>)repository.findAll();
	}

}
