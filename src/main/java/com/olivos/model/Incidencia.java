package com.olivos.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "incidencias")
public class Incidencia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer incidencia_id;

	@Column(nullable = false)
	private String descripcion;

	@Temporal(TemporalType.DATE)
	private Date fecha_registro;

	@ManyToOne
	@JoinColumn(name = "empleado_id", nullable = false, foreignKey = @ForeignKey(foreignKeyDefinition = "foreign key(empleado_id) references empleados(empleado_id)"))
	private Empleado empleado;

	@ManyToOne
	@JoinColumn(name = "alumno_id", nullable = false, foreignKey = @ForeignKey(foreignKeyDefinition = "foreign key(alumno_id) references alumnos(alumno_id)"))
	private Alumno alumno;

	@ManyToOne
	@JoinColumn(name = "curso_id", nullable = false, foreignKey = @ForeignKey(foreignKeyDefinition = "foreign key(curso_id) references cursos(curso_id)"))
	private Curso curso;

	public Incidencia() {
	}

	public Incidencia(Integer incidencia_id, String descripcion, Date fecha_registro) {
		this.incidencia_id = incidencia_id;
		this.descripcion = descripcion;
		this.fecha_registro = fecha_registro;
	}

	@PostPersist
	public void postPersist() {
		fecha_registro = new Date();
	}

	public Integer getIncidencia_id() {
		return incidencia_id;
	}

	public void setIncidencia_id(Integer incidencia_id) {
		this.incidencia_id = incidencia_id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

}
