package com.olivos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AplicacionWebCollegeApplication {

	public static void main(String[] args) {
		SpringApplication.run(AplicacionWebCollegeApplication.class, args);
	}

}
