package com.olivos.mapper;

public class RegCalificacionMapper 
{
	private Integer calificacion_id; 
	private Integer examen_final;
	private Integer practica_calificada_1; 
	private Integer practica_calificada_2; 
	private Integer practica_calificada_3;
	private Integer promedio;
	private Integer alumno_id; 
	private Integer	curso_id; 
	private Integer	periodoescolar_id;
	
	public RegCalificacionMapper() {
	}
	
	public RegCalificacionMapper(Integer calificacion_id, Integer examen_final, Integer practica_calificada_1,
			Integer practica_calificada_2, Integer practica_calificada_3, Integer promedio, Integer alumno_id,
			Integer curso_id, Integer periodoescolar_id) {
		this.calificacion_id = calificacion_id;
		this.examen_final = examen_final;
		this.practica_calificada_1 = practica_calificada_1;
		this.practica_calificada_2 = practica_calificada_2;
		this.practica_calificada_3 = practica_calificada_3;
		this.promedio = promedio;
		this.alumno_id = alumno_id;
		this.curso_id = curso_id;
		this.periodoescolar_id = periodoescolar_id;
	}
	public Integer getCalificacion_id() {
		return calificacion_id;
	}
	public void setCalificacion_id(Integer calificacion_id) {
		this.calificacion_id = calificacion_id;
	}
	public Integer getExamen_final() {
		return examen_final;
	}
	public void setExamen_final(Integer examen_final) {
		this.examen_final = examen_final;
	}
	public Integer getPractica_calificada_1() {
		return practica_calificada_1;
	}
	public void setPractica_calificada_1(Integer practica_calificada_1) {
		this.practica_calificada_1 = practica_calificada_1;
	}
	public Integer getPractica_calificada_2() {
		return practica_calificada_2;
	}
	public void setPractica_calificada_2(Integer practica_calificada_2) {
		this.practica_calificada_2 = practica_calificada_2;
	}
	public Integer getPractica_calificada_3() {
		return practica_calificada_3;
	}
	public void setPractica_calificada_3(Integer practica_calificada_3) {
		this.practica_calificada_3 = practica_calificada_3;
	}
	public Integer getPromedio() {
		return promedio;
	}
	public void setPromedio(Integer promedio) {
		this.promedio = promedio;
	}
	public Integer getAlumno_id() {
		return alumno_id;
	}
	public void setAlumno_id(Integer alumno_id) {
		this.alumno_id = alumno_id;
	}
	public Integer getCurso_id() {
		return curso_id;
	}
	public void setCurso_id(Integer curso_id) {
		this.curso_id = curso_id;
	}
	public Integer getPeriodoescolar_id() {
		return periodoescolar_id;
	}
	public void setPeriodoescolar_id(Integer periodoescolar_id) {
		this.periodoescolar_id = periodoescolar_id;
	} 
	
	
}
