package com.olivos.repository;

import org.springframework.data.repository.CrudRepository;

import com.olivos.model.PeriodoEscolar;

public interface PeriodoEscolarRepository extends CrudRepository<PeriodoEscolar, Integer>{

}
