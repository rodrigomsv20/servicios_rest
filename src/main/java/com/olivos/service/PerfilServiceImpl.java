package com.olivos.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.olivos.model.Perfil;
import com.olivos.repository.PerfilRepository;

@Service
public class PerfilServiceImpl implements PerfilService
{
	@Autowired
	private PerfilRepository repository;
	
	@Override
	@Transactional
	public void insert(Perfil perfil) {
		repository.save(perfil);
	}

	@Override
	@Transactional
	public void update(Perfil perfil) {
		repository.save(perfil);
	}

	@Override
	@Transactional
	public void delete(Integer perfil_id) {
		repository.deleteById(perfil_id);
	}

	@Override
	@Transactional(readOnly=true)
	public Perfil findById(Integer perfil_id) {
		return repository.findById(perfil_id).orElse(null);
	}

	@Override
	@Transactional(readOnly=true)
	public Collection<Perfil> findAll() {
		return (Collection<Perfil>)repository.findAll();
	}

}
