package com.olivos.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.olivos.model.Curso;
import com.olivos.repository.CursoRepository;

@Service
public class CursoServiceImpl implements CursoService{
	
	@Autowired
	private CursoRepository repository;
	
	@Override
	@Transactional
	public void insert(Curso curso) {
		repository.save(curso);
	}

	@Override
	@Transactional
	public void update(Curso curso) {
		repository.save(curso);
	}

	@Override
	@Transactional
	public void delete(Integer curso_id) {
		repository.deleteById(curso_id);
	}

	@Override
	@Transactional(readOnly=true)
	public Curso findById(Integer curso_id) {
		return repository.findById(curso_id).orElse(null);
	}

	@Override
	@Transactional(readOnly=true)
	public Collection<Curso> findAll() {
		return (Collection<Curso>)repository.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public Collection<Curso> listarCursoGrado(Integer grado_id) {
		return (Collection<Curso>)repository.listarCursoGrado(grado_id);
	}

}
