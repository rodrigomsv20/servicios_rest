package com.olivos.mapper;

import com.olivos.model.Matricula;

public class MatriculaMapper {

	private Integer grado_id;
	private Integer alumno_id;
	private String nombres;
	private String apellidos;
	
	public MatriculaMapper(Matricula m) {
		this(m.getGrado().getGrado_id(),m.getAlumno().getAlumno_id(),m.getNombres(),m.getApellidos());
	}
	
	

	public MatriculaMapper(Integer grado_id, Integer alumno_id, String nombres, String apellidos) {
		this.grado_id = grado_id;
		this.alumno_id = alumno_id;
		this.nombres = nombres;
		this.apellidos = apellidos;
	}



	public Integer getGrado_id() {
		return grado_id;
	}

	public void setGrado_id(Integer grado_id) {
		this.grado_id = grado_id;
	}

	public Integer getAlumno_id() {
		return alumno_id;
	}

	public void setAlumno_id(Integer alumno_id) {
		this.alumno_id = alumno_id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	
	
	
	
}
