package com.olivos.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.olivos.model.UsuarioEmpleado;
import com.olivos.repository.UsuarioEmpleadoRepository;

@Service
public class UsuarioEmpleadoServiceImpl implements UsuarioEmpleadoService 
{
	
	@Autowired
	private UsuarioEmpleadoRepository repository;
	
	@Override
	@Transactional
	public void insert(UsuarioEmpleado usuarioEmp) {
		repository.save(usuarioEmp);
	}

	@Override
	@Transactional
	public void update(UsuarioEmpleado usuarioEmp) {
		repository.save(usuarioEmp);
	}

	@Override
	@Transactional(readOnly=true)
	public UsuarioEmpleado findById(Integer usuario_id) {
		return repository.findById(usuario_id).orElse(null);
	}

	@Override
	@Transactional(readOnly=true)
	public Collection<UsuarioEmpleado> findAll() {
		return (Collection<UsuarioEmpleado>)repository.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public UsuarioEmpleado autentificarEmpleado(String usuario, String clave) {
		UsuarioEmpleado usuarioEmp = null;
		usuarioEmp = repository.autentificarEmpleado(usuario, clave);
		return usuarioEmp;
	}

}
