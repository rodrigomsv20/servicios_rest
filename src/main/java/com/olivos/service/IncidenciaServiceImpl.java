package com.olivos.service;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.olivos.model.Incidencia;
import com.olivos.repository.IncidenciaRepostory;

@Service
public class IncidenciaServiceImpl implements IncidenciaService{
	
	@Autowired
	private IncidenciaRepostory repository;
	
	@Override
	@Transactional
	public void insert(Incidencia incidencia) {
		repository.save(incidencia);
	}

	@Override
	@Transactional
	public void update(Incidencia incidencia) {
		repository.save(incidencia);
	}

	@Override
	@Transactional
	public void delete(Integer incidencia_id) {
		repository.deleteById(incidencia_id);
	}

	@Override
	@Transactional(readOnly = true)
	public Incidencia findById(Integer incidencia_id) {
		return repository.findById(incidencia_id).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public Collection<Incidencia> findAll() {
		return (Collection<Incidencia>)repository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Collection<Incidencia> getIncidenciaDocente(Integer empleado_id, String fecha_ini, String fecha_fin) {
		return (Collection<Incidencia>)repository.getIncidenciaDocente(empleado_id, fecha_ini, fecha_fin);
	}

	@Override
	@Transactional(readOnly = true)
	public Collection<Incidencia> getIncDocentAlum(Integer empleado_id, Integer alumno_id, String fecha_ini,
			String fecha_fin) {
		return (Collection<Incidencia>)repository.getIncDocentAlum(empleado_id, alumno_id, fecha_ini, fecha_fin);
	}

	@Override
	@Transactional(readOnly=true)
	public Collection<Incidencia> incsAlumnoFecha(Integer alumno_id, Date fecha_registro) {
		return (Collection<Incidencia>)repository.incsAlumnoFecha(alumno_id, fecha_registro);
	}

}
