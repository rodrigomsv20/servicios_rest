package com.olivos.mapper;

import com.olivos.model.Asistencia;

public class AsistenciaMapper {

	private Integer grado_id;
	private Integer alumno_id;
	private String nombres;
	private String apellidos;
	private String asistencias;

	public AsistenciaMapper(Asistencia a) {
		this(a.getGrado().getGrado_id(),a.getAlumno().getAlumno_id(),a.getAlumno().getNombres(),a.getAlumno().getApellidos(),a.getAsistencias());
	}

	public AsistenciaMapper(Integer grado_id, Integer alumno_id, String nombres, String apellidos, String asistencias) {
		this.grado_id = grado_id;
		this.alumno_id = alumno_id;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.asistencias = asistencias;
	}

	public Integer getGrado_id() {
		return grado_id;
	}

	public void setGrado_id(Integer grado_id) {
		this.grado_id = grado_id;
	}

	public Integer getAlumno_id() {
		return alumno_id;
	}

	public void setAlumno_id(Integer alumno_id) {
		this.alumno_id = alumno_id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getAsistencias() {
		return asistencias;
	}

	public void setAsistencias(String asistencias) {
		this.asistencias = asistencias;
	}

}
