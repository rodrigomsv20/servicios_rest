package com.olivos.mapper;

import java.util.Date;

import com.olivos.model.Incidencia;

public class VerIncidenciaMapper {

	private Integer incidencia_id; 
	private Integer alumno_id; 
	private Integer curso_id; 
	private Integer empleado_id;
	private String descripcion;
	private Date fecha_registro;
	private String nombre;
	
	public VerIncidenciaMapper(Incidencia i) {
		this(i.getIncidencia_id(),i.getAlumno().getAlumno_id(),i.getCurso().getCurso_id(),i.getEmpleado().getEmpleado_id(),
				i.getDescripcion(),i.getFecha_registro(),i.getCurso().getNombre());
	}
	
	public VerIncidenciaMapper(Integer incidencia_id, Integer alumno_id, Integer curso_id, Integer empleado_id,
			String descripcion, Date fecha_registro, String nombre) {
		this.incidencia_id = incidencia_id;
		this.alumno_id = alumno_id;
		this.curso_id = curso_id;
		this.empleado_id = empleado_id;
		this.descripcion = descripcion;
		this.fecha_registro = fecha_registro;
		this.nombre = nombre;
	}
	public Integer getIncidencia_id() {
		return incidencia_id;
	}
	public void setIncidencia_id(Integer incidencia_id) {
		this.incidencia_id = incidencia_id;
	}
	public Integer getAlumno_id() {
		return alumno_id;
	}
	public void setAlumno_id(Integer alumno_id) {
		this.alumno_id = alumno_id;
	}
	public Integer getCurso_id() {
		return curso_id;
	}
	public void setCurso_id(Integer curso_id) {
		this.curso_id = curso_id;
	}
	public Integer getEmpleado_id() {
		return empleado_id;
	}
	public void setEmpleado_id(Integer empleado_id) {
		this.empleado_id = empleado_id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Date getFecha_registro() {
		return fecha_registro;
	}
	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	
}
