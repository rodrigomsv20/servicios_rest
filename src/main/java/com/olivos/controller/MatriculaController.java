package com.olivos.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.olivos.mapper.MatriculaMapper;
import com.olivos.model.Matricula;
import com.olivos.service.MatriculaService;
import com.olivos.util.MapperUtil;

@RestController
@RequestMapping("/matricula")
public class MatriculaController {

	@Autowired
	private MatriculaService matriculaService;
	
	@GetMapping("/listar")
	private ResponseEntity<?> listar() {
		Collection<Matricula> itemsMatricula = matriculaService.findAll();

		if (itemsMatricula.isEmpty()) {
			return new ResponseEntity<>("No se encontro lista de matriculas", HttpStatus.NO_CONTENT);
		}
		Collection<MatriculaMapper> itemsMatriculaMapper = MapperUtil.convertMatriculaMapper(itemsMatricula);
		return new ResponseEntity<>(itemsMatriculaMapper, HttpStatus.OK);
	}
	
	@PostMapping("/registrar")
	public ResponseEntity<?> registrar(@RequestBody Matricula matricula) {

		matriculaService.insert(matricula);
		return new ResponseEntity<>("¡Se creo la matricula con éxito!", HttpStatus.CREATED);
	}
	
	@PutMapping("/editar/{matricula_id}")
	public ResponseEntity<?> editar(@RequestBody Matricula editMatricula, @PathVariable Integer matricula_id) {

		Matricula matriculaDB = matriculaService.findById(matricula_id);
		

		if (matriculaDB != null) {
			matriculaDB.setDni_apoderado(editMatricula.getDni_apoderado());
			matriculaDB.setNombres(editMatricula.getNombres());
			matriculaDB.setApellidos(editMatricula.getApellidos());
			matriculaDB.setParentes(editMatricula.getParentes());
			matriculaDB.setTelefono(editMatricula.getTelefono());
			matriculaDB.setCorreo(editMatricula.getCorreo());
			matriculaDB.setGrado(editMatricula.getGrado());
			matriculaDB.setAlumno(editMatricula.getAlumno());
			
			return new ResponseEntity<>("¡Matricula actualizado!", HttpStatus.OK);
		}

		return new ResponseEntity<>("¡Matricula no encontrado!", HttpStatus.NOT_FOUND);
	}
	
	@DeleteMapping("/borrar/{matricula_id}")
	public ResponseEntity<?> borrar(@PathVariable Integer matricula_id) {
		Matricula matriculaDB = matriculaService.findById(matricula_id);
		
		if (matriculaDB != null) {
			matriculaService.delete(matricula_id);
			return new ResponseEntity<>("¡Matricula eliminado!", HttpStatus.OK);
		}
		return new ResponseEntity<>("¡Matricula no encontrado!", HttpStatus.NOT_FOUND);
	}
	
	//Servicios Especiales
	@GetMapping("/listarAlumnoGrado/{grado_id}")
	private ResponseEntity<?> listarAlumnoGrado(@PathVariable Integer grado_id) {

		Collection<Matricula> itemsMatricula = matriculaService.listaAlumnoGrado(grado_id);
		if (itemsMatricula.isEmpty()) {
			return new ResponseEntity<>("No se encontro lista de alumnos", HttpStatus.NO_CONTENT);
		}
		Collection<MatriculaMapper> itemsMatriculaMapper = MapperUtil.convertMatriculaMapper(itemsMatricula);
		return new ResponseEntity<>(itemsMatriculaMapper, HttpStatus.OK);
	}
}
