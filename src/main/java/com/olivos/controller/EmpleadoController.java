package com.olivos.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.olivos.model.Empleado;
import com.olivos.service.EmpleadoService;

@RestController
@RequestMapping("/empleado")
public class EmpleadoController {
	@Autowired
	private EmpleadoService empleadoService;

	@GetMapping("/listar")
	private ResponseEntity<?> listar() {
		Collection<Empleado> itemsEmpleado = empleadoService.findAll();

		if (itemsEmpleado.isEmpty()) {
			return new ResponseEntity<>("No se encontro lista de empleados", HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(itemsEmpleado, HttpStatus.OK);
	}

	@PostMapping("/registrar")
	public ResponseEntity<?> registrar(@RequestBody Empleado empleado) {

		empleadoService.insert(empleado);
		return new ResponseEntity<>("¡Se creo el empleado con éxito!", HttpStatus.CREATED);
	}

	@PutMapping("/editar/{empleado_id}")
	public ResponseEntity<?> editar(@RequestBody Empleado editEmpleado, @PathVariable Integer empleado_id) {

		Empleado empleadoDB = empleadoService.findById(empleado_id);

		if (empleadoDB != null) {
			empleadoDB.setDni_empleado(editEmpleado.getDni_empleado());
			empleadoDB.setNombres(editEmpleado.getNombres());
			empleadoDB.setApellido(editEmpleado.getApellido());
			empleadoDB.setFecha_nacimiento(editEmpleado.getFecha_nacimiento());
			empleadoDB.setSexo(editEmpleado.getSexo());
			empleadoDB.setTelefono(editEmpleado.getTelefono());
			empleadoDB.setCorreo(editEmpleado.getCorreo());
			empleadoDB.setProfesion(editEmpleado.getProfesion());
			empleadoDB.setSueldo(editEmpleado.getSueldo());
			return new ResponseEntity<>("¡Empleado actualizado!", HttpStatus.OK);
		}

		return new ResponseEntity<>("¡Empleado no encontrado!", HttpStatus.NOT_FOUND);
	}

	@DeleteMapping("/borrar/{empleado_id}")
	public ResponseEntity<?> borrar(@PathVariable Integer empleado_id) {
		Empleado empleadoDB = empleadoService.findById(empleado_id);

		if (empleadoDB != null) {
			empleadoService.delete(empleado_id);
			return new ResponseEntity<>("¡Empleado eliminado!", HttpStatus.OK);
		}
		return new ResponseEntity<>("¡Empleado no encontrado!", HttpStatus.NOT_FOUND);
	}

}
