package com.olivos.service;

import java.util.Collection;

import com.olivos.model.Alumno;

public interface AlumnoService {
	public abstract void insert(Alumno alumno);
	public abstract void update(Alumno alumno);
	public abstract void delete(Integer alumno_id);
	public abstract Alumno findById(Integer alumno_id);
	public abstract Collection<Alumno> findAll();
}
