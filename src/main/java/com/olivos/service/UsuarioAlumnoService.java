package com.olivos.service;

import java.util.Collection;

import com.olivos.model.UsuarioAlumno;

public interface UsuarioAlumnoService
{
	public abstract void insert(UsuarioAlumno usuarioAlum);
	public abstract void update(UsuarioAlumno usuarioAlum);
	public abstract UsuarioAlumno findById(Integer usuarioAlum_id);
	public abstract Collection<UsuarioAlumno> findAll();

	public abstract UsuarioAlumno autentificarAlumno(String usuario,String clave);
}
