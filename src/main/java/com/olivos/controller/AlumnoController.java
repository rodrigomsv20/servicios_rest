package com.olivos.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.olivos.mapper.AlumnoMapper;
import com.olivos.model.Alumno;
import com.olivos.service.AlumnoService;
import com.olivos.util.MapperUtil;

@RestController
@RequestMapping("/alumno")
public class AlumnoController {
	
	@Autowired
	private AlumnoService alumnoService;
	
	@GetMapping("/listar")
	private ResponseEntity<?> listar() {
		Collection<Alumno> itemsAlumno = alumnoService.findAll();

		if (itemsAlumno.isEmpty()) {
			return new ResponseEntity<>("No se encontro lista de alumno", HttpStatus.NO_CONTENT);
		}
		Collection<AlumnoMapper> itemsAlumnoMapper = MapperUtil.convertAlumMapper(itemsAlumno);
		return new ResponseEntity<>(itemsAlumnoMapper, HttpStatus.OK);
	}
	
	@PostMapping("/registrar")
	public ResponseEntity<?> registrar(@RequestBody Alumno alumno) {

		alumnoService.insert(alumno);
		return new ResponseEntity<>("¡Se creo el alumno con éxito!", HttpStatus.CREATED);
	}
	
	@PutMapping("/editar/{alumno_id}")
	public ResponseEntity<?> editar(@RequestBody Alumno editAlumno, @PathVariable Integer alumno_id) {

		Alumno alumnoDB = alumnoService.findById(alumno_id);

		if (alumnoDB != null) {
			alumnoDB.setDni_alumno(editAlumno.getDni_alumno());
			alumnoDB.setNombres(editAlumno.getNombres());
			alumnoDB.setApellidos(editAlumno.getApellidos());
			alumnoDB.setFecha_nacimiento(editAlumno.getFecha_nacimiento());
			alumnoDB.setDireccion(editAlumno.getDireccion());
			
			return new ResponseEntity<>("¡Alumno actualizado!", HttpStatus.OK);
		}

		return new ResponseEntity<>("¡Alumno no encontrado!", HttpStatus.NOT_FOUND);
	}
	
	@DeleteMapping("/borrar/{alumno_id}")
	public ResponseEntity<?> borrar(@PathVariable Integer alumno_id) {
		Alumno alumnoDB = alumnoService.findById(alumno_id);

		if (alumnoDB != null) {
			alumnoService.delete(alumno_id);
			return new ResponseEntity<>("¡Alumno eliminado!", HttpStatus.OK);
		}
		return new ResponseEntity<>("¡Alumno no encontrado!", HttpStatus.NOT_FOUND);
	}

}
