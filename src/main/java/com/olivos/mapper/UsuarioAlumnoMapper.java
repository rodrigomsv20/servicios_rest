package com.olivos.mapper;

import com.olivos.model.UsuarioAlumno;

public class UsuarioAlumnoMapper {
	
	private Integer usuario_id;
	private String usuario_alumno;
	private String clave;
	private String estado;
	private Integer perfil_id;
	private Integer alumno_id;
	
	
	public UsuarioAlumnoMapper(UsuarioAlumno ua) {
		this(ua.getUsuario_id(),ua.getUsuario_alumno(),ua.getClave(),ua.getEstado(),ua.getPerfilAlumno().getPerfil_id(),ua.getAlumno().getAlumno_id());
	}
	
	public UsuarioAlumnoMapper(Integer usuario_id, String usuario_alumno, String clave, String estado,
			Integer perfil_id, Integer alumno_id) {
		this.usuario_id = usuario_id;
		this.usuario_alumno = usuario_alumno;
		this.clave = clave;
		this.estado = estado;
		this.perfil_id = perfil_id;
		this.alumno_id = alumno_id;
	}
	public Integer getUsuario_id() {
		return usuario_id;
	}
	public void setUsuario_id(Integer usuario_id) {
		this.usuario_id = usuario_id;
	}
	public String getUsuario_alumno() {
		return usuario_alumno;
	}
	public void setUsuario_alumno(String usuario_alumno) {
		this.usuario_alumno = usuario_alumno;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Integer getPerfil_id() {
		return perfil_id;
	}
	public void setPerfil_id(Integer perfil_id) {
		this.perfil_id = perfil_id;
	}
	public Integer getAlumno_id() {
		return alumno_id;
	}
	public void setAlumno_id(Integer alumno_id) {
		this.alumno_id = alumno_id;
	}
	
	
	
}
