package com.olivos.repository;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.olivos.model.Incidencia;


public interface IncidenciaRepostory extends CrudRepository<Incidencia, Integer>{
	
	@Query(value = "{call usp_getIncidenciaDocente(:empleado_id,:fecha_ini,:fecha_fin)}", nativeQuery = true)
	public abstract Collection<Incidencia> getIncidenciaDocente(@Param("empleado_id") Integer empleado_id, 
							@Param("fecha_ini") String fecha_ini, @Param("fecha_fin") String fecha_fin);
	
	@Query(value = "{call usp_getIncDocentAlum(:empleado_id,:alumno_id,:fecha_ini,:fecha_fin)}", nativeQuery = true)
	public abstract Collection<Incidencia> getIncDocentAlum(@Param("empleado_id") Integer empleado_id, @Param("alumno_id") Integer alumno_id,
							@Param("fecha_ini") String fecha_ini, @Param("fecha_fin") String fecha_fin);
	
	@Query(value = "{call usp_getIncsAlumnoFecha(:alumno_id,:fecha_registro)}", nativeQuery = true)
	public abstract Collection<Incidencia> incsAlumnoFecha(@Param("alumno_id")Integer alumno_id,@Param("fecha_registro") Date fecha_registro);
}
