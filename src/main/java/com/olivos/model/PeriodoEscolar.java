package com.olivos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "periodos_escolares")
public class PeriodoEscolar implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer periodoescolar_id;

	@Column(nullable = false)
	private String nombre;

	@OneToMany(mappedBy = "periodoEscolar")
	private Collection<Calificacion> itemsCalificacion = new ArrayList<>();

	public PeriodoEscolar() {
	}

	public PeriodoEscolar(Integer periodoescolar_id) {
		this.periodoescolar_id = periodoescolar_id;
	}
	
	public PeriodoEscolar(Integer periodoescolar_id, String nombre) {
		super();
		this.periodoescolar_id = periodoescolar_id;
		this.nombre = nombre;
	}

	public Integer getPeriodoescolar_id() {
		return periodoescolar_id;
	}

	public void setPeriodoescolar_id(Integer periodoescolar_id) {
		this.periodoescolar_id = periodoescolar_id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Collection<Calificacion> getItemsCalificacion() {
		return itemsCalificacion;
	}

	public void setItemsCalificacion(Collection<Calificacion> itemsCalificacion) {
		this.itemsCalificacion = itemsCalificacion;
	}

}
