package com.olivos.mapper;

public class RegIncidenciaMapper {

	private Integer incidencia_id;
	private Integer alumno_id;
	private Integer empleado_id;
	private Integer curso_id;
	private String descripcion;

	public RegIncidenciaMapper(Integer incidencia_id, Integer alumno_id, Integer empleado_id, Integer curso_id,
			String descripcion) {
		this.incidencia_id = incidencia_id;
		this.alumno_id = alumno_id;
		this.empleado_id = empleado_id;
		this.curso_id = curso_id;
		this.descripcion = descripcion;
	}

	public Integer getIncidencia_id() {
		return incidencia_id;
	}

	public void setIncidencia_id(Integer incidencia_id) {
		this.incidencia_id = incidencia_id;
	}

	public Integer getAlumno_id() {
		return alumno_id;
	}

	public void setAlumno_id(Integer alumno_id) {
		this.alumno_id = alumno_id;
	}

	public Integer getEmpleado_id() {
		return empleado_id;
	}

	public void setEmpleado_id(Integer empleado_id) {
		this.empleado_id = empleado_id;
	}

	public Integer getCurso_id() {
		return curso_id;
	}

	public void setCurso_id(Integer curso_id) {
		this.curso_id = curso_id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
