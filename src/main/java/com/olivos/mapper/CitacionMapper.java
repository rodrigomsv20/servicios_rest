package com.olivos.mapper;

import com.olivos.model.Citacion;

public class CitacionMapper {

	private Integer citacion_id;
	private Integer alumno_id;
	private String nombres;
	private String apellidos;
	private Integer empleado_id;
	private String fecha_cita;
	private String motivo;

	public CitacionMapper(Citacion c) {
		this(c.getCitacion_id(),c.getAlumno().getAlumno_id(),c.getAlumno().getNombres(),c.getAlumno().getApellidos(),
				c.getEmpleado().getEmpleado_id(),c.getFecha_cita(),c.getMotivo());
	}

	public CitacionMapper(Integer citacion_id, Integer alumno_id, String nombres, String apellidos, Integer empleado_id,
			String fecha_cita, String motivo) {
		this.citacion_id = citacion_id;
		this.alumno_id = alumno_id;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.empleado_id = empleado_id;
		this.fecha_cita = fecha_cita;
		this.motivo = motivo;
	}

	public Integer getCitacion_id() {
		return citacion_id;
	}

	public void setCitacion_id(Integer citacion_id) {
		this.citacion_id = citacion_id;
	}

	public Integer getAlumno_id() {
		return alumno_id;
	}

	public void setAlumno_id(Integer alumno_id) {
		this.alumno_id = alumno_id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Integer getEmpleado_id() {
		return empleado_id;
	}

	public void setEmpleado_id(Integer empleado_id) {
		this.empleado_id = empleado_id;
	}

	public String getFecha_cita() {
		return fecha_cita;
	}

	public void setFecha_cita(String fecha_cita) {
		this.fecha_cita = fecha_cita;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

}
