package com.olivos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "grados")
public class Grado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer grado_id;

	@Column(nullable = false, unique = true)
	private String grado;

	@Column(nullable = false)
	private String seccion;

	@OneToMany(mappedBy = "grado")
	private Collection<Matricula> itemsMatricula = new ArrayList<>();

	@OneToMany(mappedBy = "grado")
	private Collection<Asistencia> itemsAsistencias = new ArrayList<>();

	@OneToMany(mappedBy = "grado")
	private Collection<Tarea> itemsTarea = new ArrayList<>();

	public Grado() {

	}

	public Grado(Integer grado_id) {
		super();
		this.grado_id = grado_id;
	}

	public Grado(Integer grado_id, String grado, String seccion) {
		super();
		this.grado_id = grado_id;
		this.grado = grado;
		this.seccion = seccion;
	}

	public Integer getGrado_id() {
		return grado_id;
	}

	public void setGrado_id(Integer grado_id) {
		this.grado_id = grado_id;
	}

	public String getGrado() {
		return grado;
	}

	public void setGrado(String grado) {
		this.grado = grado;
	}

	public String getSeccion() {
		return seccion;
	}

	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}

	public Collection<Matricula> getItemsMatricula() {
		return itemsMatricula;
	}

	public void setItemsMatricula(Collection<Matricula> itemsMatricula) {
		this.itemsMatricula = itemsMatricula;
	}

	public Collection<Asistencia> getItemsAsistencias() {
		return itemsAsistencias;
	}

	public void setItemsAsistencias(Collection<Asistencia> itemsAsistencias) {
		this.itemsAsistencias = itemsAsistencias;
	}

	public Collection<Tarea> getItemsTarea() {
		return itemsTarea;
	}

	public void setItemsTarea(Collection<Tarea> itemsTarea) {
		this.itemsTarea = itemsTarea;
	}

}
