package com.olivos.controller;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.olivos.mapper.EditIncidenciaMapper;
import com.olivos.mapper.IncidenciaMapper;
import com.olivos.mapper.RegIncidenciaMapper;
import com.olivos.mapper.VerIncidenciaMapper;
import com.olivos.model.Alumno;
import com.olivos.model.Curso;
import com.olivos.model.Empleado;
import com.olivos.model.Incidencia;
import com.olivos.model.Matricula;
import com.olivos.model.Mensaje;
import com.olivos.service.IncidenciaService;
import com.olivos.service.MatriculaService;
import com.olivos.util.MapperUtil;

@RestController
@RequestMapping("/incidencia")
public class IncidenciaController {

	@Autowired
	private IncidenciaService incidenciaService;
	
	@Autowired
	private MatriculaService matriculaService;
	
	@GetMapping("/listar")
	private ResponseEntity<?> listar(){
		Collection<Incidencia> itemsIncidencia = incidenciaService.findAll();
		
		if(itemsIncidencia.isEmpty()) {
			return new ResponseEntity<>("No se encontro lista de incidencias", HttpStatus.NO_CONTENT); 
		}
		Collection<IncidenciaMapper> itemsIncidenciaMapper = MapperUtil.convertIncidenciaMapper(itemsIncidencia);
		return new ResponseEntity<>(itemsIncidenciaMapper, HttpStatus.OK);
	}
	
	@DeleteMapping("/borrar/{incidencia_id}")
	public ResponseEntity<?> borrar(@PathVariable Integer incidencia_id) {
		Incidencia incidenciaDB = incidenciaService.findById(incidencia_id);

		if (incidenciaDB != null) {
			incidenciaService.delete(incidencia_id);
			return new ResponseEntity<>("¡Incidencia eliminado!", HttpStatus.OK);
		}
		return new ResponseEntity<>("¡Incidencia no encontrado!", HttpStatus.NOT_FOUND);
	}
	
	//Servicios personalizados
	@GetMapping("/listarIncDocente/{empleado_id}")
	private ResponseEntity<?> listarIncDocente(@PathVariable Integer empleado_id) {
		
		Calendar cal= Calendar.getInstance();
		int anio = cal.get(Calendar.YEAR);
		String fecha_ini = String.valueOf(anio)+"-01-01";
		String fecha_fin = String.valueOf(anio)+"-12-31";
		System.out.println("fecha_ini: " + fecha_ini + " - fecha_fin: " + fecha_fin);
		Collection<Incidencia> itemsIncidencia = incidenciaService.getIncidenciaDocente(empleado_id, fecha_ini, fecha_fin);

		if (itemsIncidencia.isEmpty()) {
			return new ResponseEntity<>("No se encontro lista de incidencias", HttpStatus.NO_CONTENT);
		}
		Collection<IncidenciaMapper> itemsIncidenciaMapper = MapperUtil.convertIncidenciaMapper(itemsIncidencia);
		return new ResponseEntity<>(itemsIncidenciaMapper, HttpStatus.OK);
	}
	
	@PostMapping("/registrar")
	public ResponseEntity<?> registrar(@RequestBody RegIncidenciaMapper incidenciaMapp) {
		
		Incidencia incidencia = new Incidencia();
		Empleado empleado = new Empleado(incidenciaMapp.getEmpleado_id());
		Alumno alumno = new Alumno(incidenciaMapp.getAlumno_id());
		Curso curso = new Curso(incidenciaMapp.getCurso_id());
		
		incidencia.setDescripcion(incidenciaMapp.getDescripcion());
		incidencia.setEmpleado(empleado);
		incidencia.setAlumno(alumno);
		incidencia.setCurso(curso);
		
		incidenciaService.insert(incidencia);
		return new ResponseEntity<>("¡Se creo la incidencia con éxito!", HttpStatus.CREATED);
	}
	
	@GetMapping("/listarIncDocentAlum/{empleado_id}/{alumno_id}")
	private ResponseEntity<?> listarIncDocentAlum(@PathVariable Integer empleado_id,@PathVariable Integer alumno_id) {
		
		Calendar cal= Calendar.getInstance();
		int anio = cal.get(Calendar.YEAR);
		String fecha_ini = String.valueOf(anio)+"-01-01";
		String fecha_fin = String.valueOf(anio)+"-12-31";
		System.out.println("fecha_ini: " + fecha_ini + " - fecha_fin: " + fecha_fin);
		Collection<Incidencia> itemsIncidencia = incidenciaService.getIncDocentAlum(empleado_id, alumno_id, fecha_ini, fecha_fin);

		if (itemsIncidencia.isEmpty()) {
			return new ResponseEntity<>("No se encontro lista de incidencias del alumno", HttpStatus.NO_CONTENT);
		}
		Collection<IncidenciaMapper> itemsIncidenciaMapper = MapperUtil.convertIncidenciaMapper(itemsIncidencia);
		return new ResponseEntity<>(itemsIncidenciaMapper, HttpStatus.OK);
	}
	
	@GetMapping("/obtenerIncidencia/{incidencia_id}")
	public ResponseEntity<?> obtenerIncidencia(@PathVariable Integer incidencia_id) {
		Incidencia incidenciaDB = incidenciaService.findById(incidencia_id);
		if(incidenciaDB != null) {
			Matricula matriculaDB = matriculaService.getGradoAlum(incidenciaDB.getAlumno().getAlumno_id());
			EditIncidenciaMapper incidenciaMapper = new EditIncidenciaMapper(incidenciaDB.getIncidencia_id(),incidenciaDB.getDescripcion(),
													incidenciaDB.getEmpleado().getEmpleado_id(),incidenciaDB.getAlumno().getAlumno_id(),
													incidenciaDB.getCurso().getCurso_id(),incidenciaDB.getFecha_registro(),matriculaDB.getGrado().getGrado_id());			
			
			return new ResponseEntity<>(incidenciaMapper, HttpStatus.OK);
		}
		return new ResponseEntity<>("¡Incidencia no encontrado!", HttpStatus.NOT_FOUND);
	}
	
	@PutMapping("/editar")
	public ResponseEntity<?> registrar(@RequestBody EditIncidenciaMapper incidenciaMapp) {
		
		Incidencia incidencia = incidenciaService.findById(incidenciaMapp.getIncidencia_id());
		
		Empleado empleado = new Empleado(incidenciaMapp.getEmpleado_id());
		Alumno alumno = new Alumno(incidenciaMapp.getAlumno_id());
		Curso curso = new Curso(incidenciaMapp.getCurso_id());
		
		incidencia.setIncidencia_id(incidenciaMapp.getIncidencia_id());
		incidencia.setDescripcion(incidenciaMapp.getDescripcion());
		incidencia.setEmpleado(empleado);
		incidencia.setAlumno(alumno);
		incidencia.setCurso(curso);
		incidencia.setFecha_registro(incidencia.getFecha_registro());
		
		incidenciaService.update(incidencia);
		return new ResponseEntity<>("¡Se actualizo la incidencia con éxito!", HttpStatus.OK);
	}
	
	@GetMapping("/listaIncsAlumnoFecha/{alumno_id}/{fecha_registro}")
	private ResponseEntity<?> listaIncsAlumnoFecha(@PathVariable Integer alumno_id,@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Date fecha_registro){
		
		Collection<Incidencia> itemsIncidencia = incidenciaService.incsAlumnoFecha(alumno_id, fecha_registro);
		
		if(itemsIncidencia.isEmpty()) {
			Mensaje mensaje = new Mensaje("¡No se encontro incidencias registradas!");
			return new ResponseEntity<>(mensaje, HttpStatus.NOT_FOUND);
		}

		Collection<VerIncidenciaMapper> itemsVerIncidenciaMapper = MapperUtil.convertVerIncidenciaMapper(itemsIncidencia);
		return new ResponseEntity<>(itemsVerIncidenciaMapper, HttpStatus.OK);
	}
	
	
}
