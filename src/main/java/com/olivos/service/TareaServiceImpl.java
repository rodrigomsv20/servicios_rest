package com.olivos.service;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.olivos.model.Tarea;
import com.olivos.repository.TareaRepository;

@Service
public class TareaServiceImpl implements TareaService
{
	
	@Autowired
	private TareaRepository repository;
	
	@Override
	@Transactional
	public void insert(Tarea tarea) {
		repository.save(tarea);
	}

	@Override
	@Transactional
	public void update(Tarea tarea) {
		repository.save(tarea);
	}

	@Override
	@Transactional
	public void delete(Integer tarea_id) {
		repository.deleteById(tarea_id);
	}

	@Override
	@Transactional(readOnly=true)
	public Tarea findById(Integer tarea_id) {
		return repository.findById(tarea_id).orElse(null);
	}

	@Override
	@Transactional(readOnly=true)
	public Collection<Tarea> findAll() {
		return (Collection<Tarea>)repository.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public Tarea tarea(Integer grado_id, Integer curso_id, Date fecha_registro) {
		Tarea tarea = null;
		tarea = repository.tarea(grado_id, curso_id, fecha_registro);
		return tarea;
	}

	@Override
	@Transactional(readOnly=true)
	public Tarea tareaAlum(Integer empleado_id, Integer curso_id, Integer grado_id,String descripcion, String fecha_registro) {
		return repository.tareaAlum(empleado_id, curso_id, grado_id, descripcion, fecha_registro);
	}

	@Override
	@Transactional(readOnly=true)
	public Collection<Tarea> tareasGradoFecha(Integer alumno_id, Date fecha_registro) {
		return (Collection<Tarea>)repository.tareasGradoFecha(alumno_id, fecha_registro);
	}
	
}
