package com.olivos.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "usuarios_empleados")
public class UsuarioEmpleado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer usuario_id;

	@Column(nullable = false, unique = true)
	private String usuario_empleado;

	@Column
	private String clave;

	@Column
	private String estado;

	// Relación 1 a 1 entre Empleado - UsuarioEmpleado
	@OneToOne
	@JoinColumn(name = "empleado_id", nullable = false, unique = true, foreignKey = @ForeignKey(foreignKeyDefinition = "foreign key(empleado_id) references empleados(empleado_id)"))
	private Empleado empleado;

	// Relación muchos a 1 entre Perfil - UsuarioEmpleado
	@ManyToOne
	@JoinColumn(name = "perfil_id", nullable = false, foreignKey = @ForeignKey(foreignKeyDefinition = "foreign key(perfil_id) references perfiles(perfil_id)"))
	private Perfil perfilEmpleado;

	public UsuarioEmpleado() {

	}

	public UsuarioEmpleado(Integer usuario_id, String usuario_empleado, String clave, String estado) {
		super();
		this.usuario_id = usuario_id;
		this.usuario_empleado = usuario_empleado;
		this.clave = clave;
		this.estado = estado;
	}

	public Integer getUsuario_id() {
		return usuario_id;
	}

	public void setUsuario_id(Integer usuario_id) {
		this.usuario_id = usuario_id;
	}

	public String getUsuario_empleado() {
		return usuario_empleado;
	}

	public void setUsuario_empleado(String usuario_empleado) {
		this.usuario_empleado = usuario_empleado;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public Perfil getPerfilEmpleado() {
		return perfilEmpleado;
	}

	public void setPerfilEmpleado(Perfil perfil) {
		this.perfilEmpleado = perfil;
	}

}
