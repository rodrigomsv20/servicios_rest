package com.olivos.mapper;

import java.util.Date;

public class EditIncidenciaMapper {

	private Integer incidencia_id;
	private String descripcion;
	private Integer empleado_id;
	private Integer alumno_id;
	private Integer curso_id;
	private Date fecha_registro;
	private Integer grado_id;

	public EditIncidenciaMapper(Integer incidencia_id, String descripcion, Integer empleado_id, Integer alumno_id,
			Integer curso_id, Date fecha_registro, Integer grado_id) {
		this.incidencia_id = incidencia_id;
		this.descripcion = descripcion;
		this.empleado_id = empleado_id;
		this.alumno_id = alumno_id;
		this.curso_id = curso_id;
		this.fecha_registro = fecha_registro;
		this.grado_id = grado_id;
	}

	public Integer getIncidencia_id() {
		return incidencia_id;
	}

	public void setIncidencia_id(Integer incidencia_id) {
		this.incidencia_id = incidencia_id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getEmpleado_id() {
		return empleado_id;
	}

	public void setEmpleado_id(Integer empleado_id) {
		this.empleado_id = empleado_id;
	}

	public Integer getAlumno_id() {
		return alumno_id;
	}

	public void setAlumno_id(Integer alumno_id) {
		this.alumno_id = alumno_id;
	}

	public Integer getCurso_id() {
		return curso_id;
	}

	public void setCurso_id(Integer curso_id) {
		this.curso_id = curso_id;
	}

	public Date getFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

	public Integer getGrado_id() {
		return grado_id;
	}

	public void setGrado_id(Integer grado_id) {
		this.grado_id = grado_id;
	}

}
