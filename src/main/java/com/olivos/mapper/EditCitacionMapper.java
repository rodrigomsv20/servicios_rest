package com.olivos.mapper;

import java.util.Date;

public class EditCitacionMapper {

	private Integer citacion_id;
	private String fecha_cita;
	private String motivo;
	private Integer alumno_id;
	private Integer empleado_id;
	private Integer grado_id;
	private Date fecha_registro;

	public EditCitacionMapper(Integer citacion_id, String fecha_cita, String motivo, Integer alumno_id,
			Integer empleado_id, Integer grado_id, Date fecha_registro) {
		this.citacion_id = citacion_id;
		this.fecha_cita = fecha_cita;
		this.motivo = motivo;
		this.alumno_id = alumno_id;
		this.empleado_id = empleado_id;
		this.grado_id = grado_id;
		this.fecha_registro = fecha_registro;
	}

	public Integer getCitacion_id() {
		return citacion_id;
	}

	public void setCitacion_id(Integer citacion_id) {
		this.citacion_id = citacion_id;
	}

	public String getFecha_cita() {
		return fecha_cita;
	}

	public void setFecha_cita(String fecha_cita) {
		this.fecha_cita = fecha_cita;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public Integer getAlumno_id() {
		return alumno_id;
	}

	public void setAlumno_id(Integer alumno_id) {
		this.alumno_id = alumno_id;
	}

	public Integer getEmpleado_id() {
		return empleado_id;
	}

	public void setEmpleado_id(Integer empleado_id) {
		this.empleado_id = empleado_id;
	}

	public Integer getGrado_id() {
		return grado_id;
	}

	public void setGrado_id(Integer grado_id) {
		this.grado_id = grado_id;
	}

	public Date getFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

}
