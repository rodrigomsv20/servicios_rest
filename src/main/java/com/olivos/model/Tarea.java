package com.olivos.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "tareas")
public class Tarea {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer tarea_id;

	@Column(nullable = false)
	private String descripcion;

	@Temporal(TemporalType.DATE)
	private Date fecha_registro;

	@ManyToOne
	@JoinColumn(name = "empleado_id", nullable = false, foreignKey = @ForeignKey(foreignKeyDefinition = "foreign key(empleado_id) references empleados(empleado_id)"))
	private Empleado empleado;

	@ManyToOne
	@JoinColumn(name = "grado_id", nullable = false, foreignKey = @ForeignKey(foreignKeyDefinition = "foreign key(grado_id) references grados(grado_id)"))
	private Grado grado;

	@ManyToOne
	@JoinColumn(name = "curso_id", nullable = false, foreignKey = @ForeignKey(foreignKeyDefinition = "foreign key(curso_id) references cursos(curso_id)"))
	private Curso curso;

	public Tarea() {

	}

	public Tarea(Integer tarea_id, String descripcion, Date fecha_registro) {
		super();
		this.tarea_id = tarea_id;
		this.descripcion = descripcion;
		this.fecha_registro = fecha_registro;
	}

	@PostPersist
	public void postPersist() {
		fecha_registro = new Date();
	}

	public Integer getTarea_id() {
		return tarea_id;
	}

	public void setTarea_id(Integer tarea_id) {
		this.tarea_id = tarea_id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public Grado getGrado() {
		return grado;
	}

	public void setGrado(Grado grado) {
		this.grado = grado;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

}
