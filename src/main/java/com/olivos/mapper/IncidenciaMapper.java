package com.olivos.mapper;

import com.olivos.model.Incidencia;

public class IncidenciaMapper {

	private Integer incidencia_id;
	private Integer alumno_id;
	private String nombres; // Nombre de alumno
	private String apellidos; // Apellidos del alumno
	private Integer curso_id;
	private String nombre;
	private String descripcion;

	public IncidenciaMapper(Incidencia i) {
		this(i.getIncidencia_id(), i.getAlumno().getAlumno_id(), i.getAlumno().getNombres(),
				i.getAlumno().getApellidos(), i.getCurso().getCurso_id(), i.getCurso().getNombre(), i.getDescripcion());
	}

	public IncidenciaMapper(Integer incidencia_id, Integer alumno_id, String nombres, String apellidos,
			Integer curso_id, String nombre, String descripcion) {
		this.incidencia_id = incidencia_id;
		this.alumno_id = alumno_id;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.curso_id = curso_id;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	public Integer getIncidencia_id() {
		return incidencia_id;
	}

	public void setIncidencia_id(Integer incidencia_id) {
		this.incidencia_id = incidencia_id;
	}

	public Integer getAlumno_id() {
		return alumno_id;
	}

	public void setAlumno_id(Integer alumno_id) {
		this.alumno_id = alumno_id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Integer getCurso_id() {
		return curso_id;
	}

	public void setCurso_id(Integer curso_id) {
		this.curso_id = curso_id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
