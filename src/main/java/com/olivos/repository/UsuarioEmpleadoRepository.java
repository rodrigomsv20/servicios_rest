package com.olivos.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.olivos.model.UsuarioEmpleado;

public interface UsuarioEmpleadoRepository extends CrudRepository<UsuarioEmpleado, Integer>{
	
	@Query(value = "{call usp_getAutentificarEmpleado(:usuario,:clave)}", nativeQuery = true)
	public abstract UsuarioEmpleado autentificarEmpleado(@Param("usuario") String usuario, @Param("clave") String clave);
	
}
