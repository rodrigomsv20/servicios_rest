package com.olivos.mapper;

import java.util.Date;

import com.olivos.model.Alumno;

public class AlumnoMapper {

	private Integer alumno_id;
	private Integer dni_alumno;
	private String nombres;
	private String apellidos;
	private String fecha_nacimiento;
	private String direccion;
	private Date fecha_registro;
	private String usuario_alumno;
	private String estado;
	
	public AlumnoMapper(Alumno a) {
		this(a.getAlumno_id(),a.getDni_alumno(),a.getNombres(),a.getApellidos(),a.getFecha_nacimiento(),a.getDireccion(),
				a.getFecha_registro(),a.getUsuarioAlumno().getUsuario_alumno(),a.getUsuarioAlumno().getEstado());
	}
	
	public AlumnoMapper(Integer alumno_id, Integer dni_alumno, String nombres, String apellidos,
			String fecha_nacimiento, String direccion, Date fecha_registro, String usuario_alumno, String estado) {
		this.alumno_id = alumno_id;
		this.dni_alumno = dni_alumno;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.fecha_nacimiento = fecha_nacimiento;
		this.direccion = direccion;
		this.fecha_registro = fecha_registro;
		this.usuario_alumno = usuario_alumno;
		this.estado = estado;
	}

	public Integer getAlumno_id() {
		return alumno_id;
	}

	public void setAlumno_id(Integer alumno_id) {
		this.alumno_id = alumno_id;
	}

	public Integer getDni_alumno() {
		return dni_alumno;
	}

	public void setDni_alumno(Integer dni_alumno) {
		this.dni_alumno = dni_alumno;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(String fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Date getFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

	public String getUsuario_alumno() {
		return usuario_alumno;
	}

	public void setUsuario_alumno(String usuario_alumno) {
		this.usuario_alumno = usuario_alumno;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

}
