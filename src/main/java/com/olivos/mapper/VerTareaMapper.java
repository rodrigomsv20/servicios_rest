package com.olivos.mapper;

import java.util.Date;

import com.olivos.model.Tarea;

public class VerTareaMapper {

	private Integer empleado_id;
	private Integer grado_id;
	private Integer curso_id;
	private String descripcion;
	private Date fecha_registro;
	private String nombre;

	public VerTareaMapper(Tarea t) {
		this(t.getEmpleado().getEmpleado_id(), t.getGrado().getGrado_id(), t.getCurso().getCurso_id(),
				t.getDescripcion(), t.getFecha_registro(), t.getCurso().getNombre());
	}

	public VerTareaMapper(Integer empleado_id, Integer grado_id, Integer curso_id, String descripcion,
			Date fecha_registro, String nombre) {
		this.empleado_id = empleado_id;
		this.grado_id = grado_id;
		this.curso_id = curso_id;
		this.descripcion = descripcion;
		this.fecha_registro = fecha_registro;
		this.nombre = nombre;
	}

	public Integer getEmpleado_id() {
		return empleado_id;
	}

	public void setEmpleado_id(Integer empleado_id) {
		this.empleado_id = empleado_id;
	}

	public Integer getGrado_id() {
		return grado_id;
	}

	public void setGrado_id(Integer grado_id) {
		this.grado_id = grado_id;
	}

	public Integer getCurso_id() {
		return curso_id;
	}

	public void setCurso_id(Integer curso_id) {
		this.curso_id = curso_id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
