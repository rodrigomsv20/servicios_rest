package com.olivos.repository;

import org.springframework.data.repository.CrudRepository;

import com.olivos.model.Empleado;

public interface EmpleadoRepository extends CrudRepository<Empleado, Integer>{

}
