package com.olivos.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.olivos.mapper.UsuarioAlumnoMapper;
import com.olivos.model.UsuarioAlumno;
import com.olivos.service.UsuarioAlumnoService;
import com.olivos.util.MapperUtil;

@RestController
@RequestMapping("/usuarioalumno")
public class UsuarioAlumnoController
{
	
	@Autowired
	private UsuarioAlumnoService usuarioAlumnoService;
	
	@GetMapping("/listar")
	private ResponseEntity<?> listar() {
		Collection<UsuarioAlumno> itemsUsuario = usuarioAlumnoService.findAll();

		if (itemsUsuario.isEmpty()) {
			return new ResponseEntity<>("No se encontro lista de usuarios", HttpStatus.NO_CONTENT);
		}
		Collection<UsuarioAlumnoMapper> itemsUsuarioMapper = MapperUtil.convertUsuaAlumMapper(itemsUsuario);
		return new ResponseEntity<>(itemsUsuarioMapper, HttpStatus.OK);
	}
	
	
	@PostMapping("/registrar")
	public ResponseEntity<?> registrar(@RequestBody UsuarioAlumno usuario) {

		usuarioAlumnoService.insert(usuario);
		return new ResponseEntity<>("¡Se creo el usuario con éxito!", HttpStatus.CREATED);
	}
	
	@PutMapping("/editar/{usuario_id}")
	public ResponseEntity<?> editar(@RequestBody UsuarioAlumno editUsuarioAlum, @PathVariable Integer usuario_id) {

		UsuarioAlumno usuarioAlumDB = usuarioAlumnoService.findById(usuario_id);

		if (usuarioAlumDB != null) {
			usuarioAlumDB.setUsuario_alumno(editUsuarioAlum.getUsuario_alumno());
			usuarioAlumDB.setClave(editUsuarioAlum.getClave());
			usuarioAlumDB.setPerfilAlumno(editUsuarioAlum.getPerfilAlumno());
			usuarioAlumDB.setAlumno(editUsuarioAlum.getAlumno());
			
			return new ResponseEntity<>("¡Usuario actualizado!", HttpStatus.OK);
		}

		return new ResponseEntity<>("¡Usuario no encontrado!", HttpStatus.NOT_FOUND);
	}
}
