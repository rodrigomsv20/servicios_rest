package com.olivos.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.olivos.mapper.GradoMapper;
import com.olivos.model.Grado;
import com.olivos.service.GradoService;
import com.olivos.util.MapperUtil;

@RestController
@RequestMapping("/grado")
public class GradoController {
	
	@Autowired
	private GradoService gradoService;
	
	@GetMapping("/listar")
	private ResponseEntity<?> listar() {
		Collection<Grado> itemsGrado = gradoService.findAll();

		if (itemsGrado.isEmpty()) {
			return new ResponseEntity<>("No se encontro lista de grado", HttpStatus.NO_CONTENT);
		}
		Collection<GradoMapper> itemsGradoMapper = MapperUtil.convertGradoMapper(itemsGrado);
		return new ResponseEntity<>(itemsGradoMapper, HttpStatus.OK);
	}
	
	@PostMapping("/registrar")
	public ResponseEntity<?> registrar(@RequestBody Grado grado) {

		gradoService.insert(grado);
		return new ResponseEntity<>("¡Se creo el grado con éxito!", HttpStatus.CREATED);
	}
	
	@PutMapping("/editar/{grado_id}")
	public ResponseEntity<?> editar(@RequestBody Grado editGrado, @PathVariable Integer grado_id) {

		Grado gradoDB = gradoService.findById(grado_id);

		if (gradoDB != null) {
			gradoDB.setGrado(editGrado.getGrado());
			gradoDB.setSeccion(editGrado.getSeccion());
			
			return new ResponseEntity<>("¡Grado actualizado!", HttpStatus.OK);
		}

		return new ResponseEntity<>("¡Grado no encontrado!", HttpStatus.NOT_FOUND);
	}
	
	@DeleteMapping("/borrar/{grado_id}")
	public ResponseEntity<?> borrar(@PathVariable Integer grado_id) {
		Grado gradoDB = gradoService.findById(grado_id);
		
		if (gradoDB != null) {
			gradoService.delete(grado_id);
			return new ResponseEntity<>("¡Grado eliminado!", HttpStatus.OK);
		}
		return new ResponseEntity<>("¡Grado no encontrado!", HttpStatus.NOT_FOUND);
	}
}
