package com.olivos.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.olivos.model.Matricula;
import com.olivos.repository.MatriculaRepository;

@Service
public class MatriculaServiceImpl implements MatriculaService{
	
	@Autowired
	private MatriculaRepository repository;
	
	@Override
	@Transactional
	public void insert(Matricula matricula) {
		repository.save(matricula);
	}

	@Override
	@Transactional
	public void update(Matricula matricula) {
		repository.save(matricula);
	}

	@Override
	@Transactional
	public void delete(Integer matricula_id) {
		repository.deleteById(matricula_id);
	}

	@Override
	@Transactional(readOnly=true)
	public Matricula findById(Integer matricula_id) {
		return repository.findById(matricula_id).orElse(null);
	}

	@Override
	@Transactional(readOnly=true)
	public Collection<Matricula> findAll() {
		return (Collection<Matricula>)repository.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public Collection<Matricula> listaAlumnoGrado(Integer grado_id) {
		return (Collection<Matricula>)repository.listaAlumnoGrado(grado_id);
	}

	@Override
	@Transactional(readOnly=true)
	public Matricula getGradoAlum(Integer grado_id) {
		return repository.getGradoAlum(grado_id);
	}

}
