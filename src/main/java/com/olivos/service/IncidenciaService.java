package com.olivos.service;

import java.util.Collection;
import java.util.Date;


import com.olivos.model.Incidencia;

public interface IncidenciaService {
	public abstract void insert(Incidencia incidencia);
	public abstract void update(Incidencia incidencia);
	public abstract void delete(Integer incidencia_id);
	public abstract Incidencia findById(Integer incidencia_id);
	public abstract Collection<Incidencia> findAll();
	
	public abstract Collection<Incidencia> getIncidenciaDocente(Integer empleado_id, String fecha_ini,String fecha_fin);
	public abstract Collection<Incidencia> getIncDocentAlum(Integer empleado_id, Integer alumno_id,String fecha_ini, String fecha_fin);
	public abstract Collection<Incidencia> incsAlumnoFecha(Integer alumno_id,Date fecha_registro);
}
