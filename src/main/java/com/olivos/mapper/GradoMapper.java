package com.olivos.mapper;

import com.olivos.model.Grado;

public class GradoMapper {

	private Integer grado_id;
	private String grado;
	private String seccion;
	
	public GradoMapper(Grado g) {
		this(g.getGrado_id(),g.getGrado(),g.getSeccion());
	}
	
	public GradoMapper(Integer grado_id, String grado, String seccion) {
		super();
		this.grado_id = grado_id;
		this.grado = grado;
		this.seccion = seccion;
	}

	public Integer getGrado_id() {
		return grado_id;
	}

	public void setGrado_id(Integer grado_id) {
		this.grado_id = grado_id;
	}

	public String getGrado() {
		return grado;
	}

	public void setGrado(String grado) {
		this.grado = grado;
	}

	public String getSeccion() {
		return seccion;
	}

	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}

}
