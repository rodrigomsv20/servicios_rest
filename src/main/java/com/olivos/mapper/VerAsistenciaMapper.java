package com.olivos.mapper;

import java.util.Date;

import com.olivos.model.Asistencia;

public class VerAsistenciaMapper {

	private Integer asistencia_id;
	private Integer alumno_id;
	private Integer curso_id;
	private Integer grado_id;
	private String asistencias;
	private Date fecha_registro;
	private String nombre;

	public VerAsistenciaMapper(Asistencia a) {
		this(a.getAsistencia_id(), a.getAlumno().getAlumno_id(), a.getCurso().getCurso_id(), a.getGrado().getGrado_id(),
				a.getAsistencias(), a.getFecha_registro(),a.getCurso().getNombre());
	}

	public VerAsistenciaMapper(Integer asistencia_id, Integer alumno_id, Integer curso_id, Integer grado_id,
			String asistencias, Date fecha_registro, String nombre) {
		super();
		this.asistencia_id = asistencia_id;
		this.alumno_id = alumno_id;
		this.curso_id = curso_id;
		this.grado_id = grado_id;
		this.asistencias = asistencias;
		this.fecha_registro = fecha_registro;
		this.nombre = nombre;
	}

	public Integer getAsistencia_id() {
		return asistencia_id;
	}

	public void setAsistencia_id(Integer asistencia_id) {
		this.asistencia_id = asistencia_id;
	}

	public Integer getAlumno_id() {
		return alumno_id;
	}

	public void setAlumno_id(Integer alumno_id) {
		this.alumno_id = alumno_id;
	}

	public Integer getCurso_id() {
		return curso_id;
	}

	public void setCurso_id(Integer curso_id) {
		this.curso_id = curso_id;
	}

	public Integer getGrado_id() {
		return grado_id;
	}

	public void setGrado_id(Integer grado_id) {
		this.grado_id = grado_id;
	}

	public String getAsistencias() {
		return asistencias;
	}

	public void setAsistencias(String asistencias) {
		this.asistencias = asistencias;
	}

	public Date getFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
