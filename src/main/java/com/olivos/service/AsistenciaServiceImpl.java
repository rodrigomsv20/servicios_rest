package com.olivos.service;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.olivos.model.Asistencia;
import com.olivos.repository.AsistenciaRepository;

@Service
public class AsistenciaServiceImpl implements AsistenciaService{
	
	@Autowired
	private AsistenciaRepository repository;
	
	@Override
	@Transactional
	public void insert(Asistencia asistencia) {
		repository.save(asistencia);
	}

	@Override
	@Transactional
	public void update(Asistencia asistencia) {
		repository.save(asistencia);
	}

	@Override
	@Transactional
	public void delete(Integer asistencia_id) {
		repository.deleteById(asistencia_id);
	}

	@Override
	@Transactional(readOnly=true)
	public Asistencia findById(Integer asistencia_id) {
		return repository.findById(asistencia_id).orElse(null);
	}

	@Override
	@Transactional(readOnly=true)
	public Collection<Asistencia> findAll() {
		return (Collection<Asistencia>)repository.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public Collection<Asistencia> listaAsistenciaAlumnos(Integer grado_id, Integer curso_id, Date fecha_registro) {
		return (Collection<Asistencia>)repository.listaAsistenciaAlumnos(grado_id, curso_id, fecha_registro);
	}

	@Override
	@Transactional(readOnly=true)
	public Asistencia asistenciAlum(Integer alumno_id, Integer curso_id, Integer grado_id, String asistencias,
			String fecha_registro) {
		return repository.asistenciAlum(alumno_id, curso_id, grado_id, asistencias, fecha_registro);
		
	}

	@Override
	@Transactional(readOnly=true)
	public Collection<Asistencia> asisAlumnoFecha(Integer alumno_id, Date fecha_registro) {
		return (Collection<Asistencia>)repository.asisAlumnoFecha(alumno_id, fecha_registro);
	}

}
