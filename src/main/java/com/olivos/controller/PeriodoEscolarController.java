package com.olivos.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.olivos.mapper.PeriodoEscolarMapper;
import com.olivos.model.PeriodoEscolar;
import com.olivos.service.PeriodoEscolarService;
import com.olivos.util.MapperUtil;

@RestController
@RequestMapping("/periodoescolar")
public class PeriodoEscolarController {
	
	@Autowired
	private PeriodoEscolarService periodoEscolarService;
	
	@GetMapping("/listar")
	private ResponseEntity<?> listar() {
		Collection<PeriodoEscolar> itemsPeriodoEscolar = periodoEscolarService.findAll();

		if (itemsPeriodoEscolar.isEmpty()) {
			return new ResponseEntity<>("No se encontro lista de periodos escolares", HttpStatus.NO_CONTENT);
		}
		Collection<PeriodoEscolarMapper> itemsPeriodoEscolarMapper = MapperUtil.convertPeriodoEscolarMapper(itemsPeriodoEscolar);
		return new ResponseEntity<>(itemsPeriodoEscolarMapper, HttpStatus.OK);
	}
	
}
