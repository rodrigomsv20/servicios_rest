package com.olivos.repository;

import org.springframework.data.repository.CrudRepository;

import com.olivos.model.Alumno;

public interface AlumnoRepository extends CrudRepository<Alumno, Integer>{

}
