package com.olivos.repository;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.olivos.model.Citacion;

public interface CitacionRepository extends CrudRepository<Citacion, Integer>{
	
	@Query(value = "{call usp_getCitacionDocente(:empleado_id,:fecha_ini,:fecha_fin)}", nativeQuery = true)
	public abstract Collection<Citacion> getCitacionDocente(@Param("empleado_id") Integer empleado_id, 
							@Param("fecha_ini") String fecha_ini, @Param("fecha_fin") String fecha_fin);
	
	@Query(value = "{call usp_getCitDocentAlum(:empleado_id,:alumno_id,:fecha_ini,:fecha_fin)}", nativeQuery = true)
	public abstract Collection<Citacion> getCitDocentAlum(@Param("empleado_id") Integer empleado_id, @Param("alumno_id") Integer alumno_id,
							@Param("fecha_ini") String fecha_ini, @Param("fecha_fin") String fecha_fin);
	
	@Query(value = "{call usp_getCitasAlumnoFecha(:alumno_id,:fecha_cita)}", nativeQuery = true)
	public abstract Collection<Citacion> citasAlumnoFecha( @Param("alumno_id") Integer alumno_id, @Param("fecha_cita") Date fecha_cita);
}
