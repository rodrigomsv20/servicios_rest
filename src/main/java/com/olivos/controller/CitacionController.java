package com.olivos.controller;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.olivos.mapper.CitacionMapper;
import com.olivos.mapper.EditCitacionMapper;
import com.olivos.mapper.RegCitacionMapper;
import com.olivos.mapper.VerCitacionMapper;
import com.olivos.model.Alumno;
import com.olivos.model.Citacion;
import com.olivos.model.Empleado;
import com.olivos.model.Matricula;
import com.olivos.service.CitacionService;
import com.olivos.service.MatriculaService;
import com.olivos.util.MapperUtil;

@RestController
@RequestMapping("/citacion")
public class CitacionController {
	
	@Autowired
	private CitacionService citacionService;
	
	@Autowired
	private MatriculaService matriculaService;
	
	@GetMapping("/listarCitDocente/{empleado_id}")
	private ResponseEntity<?> listarCitDocente(@PathVariable Integer empleado_id) {
		
		Calendar cal= Calendar.getInstance();
		int anio = cal.get(Calendar.YEAR);
		String fecha_ini = String.valueOf(anio)+"-01-01";
		String fecha_fin = String.valueOf(anio)+"-12-31";
		System.out.println("fecha_ini: " + fecha_ini + " - fecha_fin: " + fecha_fin);
		
		Collection<Citacion> itemsCitacion = citacionService.getCitacionDocente(empleado_id, fecha_ini, fecha_fin);

		if (itemsCitacion.isEmpty()) {
			return new ResponseEntity<>("No se encontro lista de citaciones", HttpStatus.NO_CONTENT);
		}
		Collection<CitacionMapper> itemsCitacionMapper = MapperUtil.convertCitacionMapper(itemsCitacion);
		return new ResponseEntity<>(itemsCitacionMapper, HttpStatus.OK);
	}
	
	@GetMapping("/listarCitDocentAlum/{empleado_id}/{alumno_id}")
	private ResponseEntity<?> listarCitDocentAlum(@PathVariable Integer empleado_id,@PathVariable Integer alumno_id) {
		
		Calendar cal= Calendar.getInstance();
		int anio = cal.get(Calendar.YEAR);
		String fecha_ini = String.valueOf(anio)+"-01-01";
		String fecha_fin = String.valueOf(anio)+"-12-31";
		System.out.println("fecha_ini: " + fecha_ini + " - fecha_fin: " + fecha_fin);
		Collection<Citacion> itemsCitacion = citacionService.getCitDocentAlum(empleado_id, alumno_id, fecha_ini, fecha_fin);

		if (itemsCitacion.isEmpty()) {
			return new ResponseEntity<>("No se encontro lista de citaciones del alumno", HttpStatus.NO_CONTENT);
		}
		Collection<CitacionMapper> itemsCitacionMapper = MapperUtil.convertCitacionMapper(itemsCitacion);
		return new ResponseEntity<>(itemsCitacionMapper, HttpStatus.OK);
	}
	
	@PostMapping("/registrar")
	public ResponseEntity<?> registrar(@RequestBody RegCitacionMapper citacionMapp) {
		
		Citacion citacion = new Citacion();
		Empleado empleado = new Empleado(citacionMapp.getEmpleado_id());
		Alumno alumno = new Alumno(citacionMapp.getAlumno_id());
		
		citacion.setEmpleado(empleado);
		citacion.setAlumno(alumno);
		citacion.setMotivo(citacionMapp.getMotivo());
		citacion.setFecha_cita(citacionMapp.getFecha_cita());

		citacionService.insert(citacion);
		return new ResponseEntity<>("¡Se creo la citación con éxito!", HttpStatus.CREATED);
	}
	
	@GetMapping("/obtenerCitacion/{citacion_id}")
	public ResponseEntity<?> obtenerCitacion(@PathVariable Integer citacion_id) {
		Citacion citacionDB = citacionService.findById(citacion_id);
		if(citacionDB != null) {
			Matricula matriculaDB = matriculaService.getGradoAlum(citacionDB.getAlumno().getAlumno_id());
			EditCitacionMapper citacionMapper = new EditCitacionMapper(
					citacionDB.getCitacion_id(),
					citacionDB.getFecha_cita(),
					citacionDB.getMotivo(),
					citacionDB.getAlumno().getAlumno_id(),
					citacionDB.getEmpleado().getEmpleado_id(),
					matriculaDB.getGrado().getGrado_id(),
					citacionDB.getFecha_registro());
			
			return new ResponseEntity<>(citacionMapper, HttpStatus.OK);
		}
		return new ResponseEntity<>("¡Citación no encontrado!", HttpStatus.NOT_FOUND);
	}
	
	@PutMapping("/editar")
	public ResponseEntity<?> registrar(@RequestBody EditCitacionMapper citacionMapper) {
		
		Citacion citacion = citacionService.findById(citacionMapper.getCitacion_id());
		
		Empleado empleado = new Empleado(citacionMapper.getEmpleado_id());
		Alumno alumno = new Alumno(citacionMapper.getAlumno_id());
		
		citacion.setCitacion_id(citacionMapper.getCitacion_id());
		citacion.setMotivo(citacionMapper.getMotivo());
		citacion.setFecha_cita(citacionMapper.getFecha_cita());
		citacion.setFecha_registro(citacion.getFecha_registro());
		citacion.setEmpleado(empleado);
		citacion.setAlumno(alumno);

		citacionService.update(citacion);
		return new ResponseEntity<>("¡Se actualizo la citación con éxito!", HttpStatus.OK);
	}
	
	@DeleteMapping("/borrar/{citacion_id}")
	public ResponseEntity<?> borrar(@PathVariable Integer citacion_id) {
		Citacion citacionDB = citacionService.findById(citacion_id);

		if (citacionDB != null) {
			citacionService.delete(citacion_id);
			return new ResponseEntity<>("¡Citación eliminado!", HttpStatus.OK);
		}
		return new ResponseEntity<>("¡Citación no encontrado!", HttpStatus.NOT_FOUND);
	}
	
	@GetMapping("/listarCitasAlumnoFecha/{alumno_id}/{fecha_cita}")
	private ResponseEntity<?> listarCitasAlumnoFecha(@PathVariable Integer alumno_id,@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Date fecha_cita) {
		
		Collection<Citacion> itemsCitacion = citacionService.citasAlumnoFecha(alumno_id, fecha_cita);

		if (itemsCitacion.isEmpty()) {
			return new ResponseEntity<>("No se encontro lista de citaciones del alumno", HttpStatus.NO_CONTENT);
		}
		Collection<VerCitacionMapper> itemsVerCitacionMapper = MapperUtil.convertVerCitacionMapper(itemsCitacion);
		return new ResponseEntity<>(itemsVerCitacionMapper, HttpStatus.OK);
	}
}
