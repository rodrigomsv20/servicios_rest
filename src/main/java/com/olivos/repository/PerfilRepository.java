package com.olivos.repository;

import org.springframework.data.repository.CrudRepository;

import com.olivos.model.Perfil;

public interface PerfilRepository extends CrudRepository<Perfil, Integer>{

}
