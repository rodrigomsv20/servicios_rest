package com.olivos.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "citaciones")
public class Citacion implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer citacion_id;

	@Column(nullable = false)
	private String motivo;

	@Column(nullable = false)
	private String fecha_cita;

	@Temporal(TemporalType.DATE)
	private Date fecha_registro;

	@ManyToOne
	@JoinColumn(name = "alumno_id", nullable = false, foreignKey = @ForeignKey(foreignKeyDefinition = "foreign key(alumno_id) references alumnos(alumno_id)"))
	private Alumno alumno;

	@ManyToOne
	@JoinColumn(name = "empleado_id", nullable = false, foreignKey = @ForeignKey(foreignKeyDefinition = "foreign key(empleado_id) references empleados(empleado_id)"))
	private Empleado empleado;

	public Citacion() {

	}

	public Citacion(Integer citacion_id, String motivo, String fecha_cita, Date fecha_registro) {
		this.citacion_id = citacion_id;
		this.motivo = motivo;
		this.fecha_cita = fecha_cita;
		this.fecha_registro = fecha_registro;
	}

	@PostPersist
	public void postPersist() {
		fecha_registro = new Date();
	}

	public Integer getCitacion_id() {
		return citacion_id;
	}

	public void setCitacion_id(Integer citacion_id) {
		this.citacion_id = citacion_id;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getFecha_cita() {
		return fecha_cita;
	}

	public void setFecha_cita(String fecha_cita) {
		this.fecha_cita = fecha_cita;
	}

	public Date getFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

}
