package com.olivos.mapper;

import com.olivos.model.Perfil;

public class PerfilMapper {

	private Integer perfil_id;
	private String tipo;

	public PerfilMapper(Perfil p) {
		this(p.getPerfil_id(), p.getTipo());
	}

	public PerfilMapper(Integer perfil_id, String tipo) {
		this.perfil_id = perfil_id;
		this.tipo = tipo;
	}

	public Integer getPerfil_id() {
		return perfil_id;
	}

	public void setPerfil_id(Integer perfil_id) {
		this.perfil_id = perfil_id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
