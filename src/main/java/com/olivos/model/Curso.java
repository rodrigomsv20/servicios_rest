package com.olivos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "cursos")
public class Curso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer curso_id;

	@Column(nullable = false)
	private String nombre;

	@OneToMany(mappedBy = "curso")
	private Collection<Asistencia> itemsAsistencias = new ArrayList<>();

	@OneToMany(mappedBy = "curso")
	private Collection<Tarea> itemsTarea = new ArrayList<>();

	@OneToMany(mappedBy = "curso")
	private Collection<Incidencia> itemsIncidencia = new ArrayList<>();

	@OneToMany(mappedBy = "curso")
	private Collection<Calificacion> itemsCalificacion = new ArrayList<>();

	public Curso() {
	}

	public Curso(Integer curso_id) {
		super();
		this.curso_id = curso_id;
	}

	public Curso(Integer curso_id, String nombre) {
		this.curso_id = curso_id;
		this.nombre = nombre;
	}

	public Integer getCurso_id() {
		return curso_id;
	}

	public void setCurso_id(Integer curso_id) {
		this.curso_id = curso_id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Collection<Asistencia> getItemsAsistencias() {
		return itemsAsistencias;
	}

	public void setItemsAsistencias(Collection<Asistencia> itemsAsistencias) {
		this.itemsAsistencias = itemsAsistencias;
	}

	public Collection<Tarea> getItemsTarea() {
		return itemsTarea;
	}

	public void setItemsTarea(Collection<Tarea> itemsTarea) {
		this.itemsTarea = itemsTarea;
	}

	public Collection<Incidencia> getItemsIncidencia() {
		return itemsIncidencia;
	}

	public void setItemsIncidencia(Collection<Incidencia> itemsIncidencia) {
		this.itemsIncidencia = itemsIncidencia;
	}

	public Collection<Calificacion> getItemsCalificacion() {
		return itemsCalificacion;
	}

	public void setItemsCalificacion(Collection<Calificacion> itemsCalificacion) {
		this.itemsCalificacion = itemsCalificacion;
	}

}
