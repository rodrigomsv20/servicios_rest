package com.olivos.repository;

import org.springframework.data.repository.CrudRepository;

import com.olivos.model.Grado;

public interface GradoRepository extends CrudRepository<Grado, Integer>{

}
