package com.olivos.service;

import java.util.Collection;

import com.olivos.model.Matricula;

public interface MatriculaService {
	public abstract void insert(Matricula matricula);
	public abstract void update(Matricula matricula);
	public abstract void delete(Integer matricula_id);
	public abstract Matricula findById(Integer matricula_id);
	public abstract Collection<Matricula> findAll();
	
	public abstract Collection<Matricula> listaAlumnoGrado(Integer grado_id);
	public abstract Matricula getGradoAlum(Integer grado_id);
}
