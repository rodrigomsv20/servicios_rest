package com.olivos.mapper;

import com.olivos.model.Curso;

public class CursoMapper {

	private Integer curso_id;
	private String nombre;

	public CursoMapper(Curso c) {
		this(c.getCurso_id(),c.getNombre());
	}

	public CursoMapper(Integer curso_id, String nombre) {
		this.curso_id = curso_id;
		this.nombre = nombre;
	}

	public Integer getCurso_id() {
		return curso_id;
	}

	public void setCurso_id(Integer curso_id) {
		this.curso_id = curso_id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
