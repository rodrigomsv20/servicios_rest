package com.olivos.service;

import java.util.Collection;

import com.olivos.model.PeriodoEscolar;

public interface PeriodoEscolarService {
	public abstract void insert(PeriodoEscolar periodoEscolar);
	public abstract void update(PeriodoEscolar periodoEscolar);
	public abstract void delete(Integer periodoescolar_id);
	public abstract PeriodoEscolar findById(Integer periodoescolar_id);
	public abstract Collection<PeriodoEscolar> findAll();
}
