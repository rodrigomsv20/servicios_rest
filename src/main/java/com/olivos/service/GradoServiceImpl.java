package com.olivos.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.olivos.model.Grado;
import com.olivos.repository.GradoRepository;

@Service
public class GradoServiceImpl implements GradoService{
	
	@Autowired
	private GradoRepository repository;
	
	@Override
	@Transactional
	public void insert(Grado grado) {
		repository.save(grado);
	}

	@Override
	@Transactional
	public void update(Grado grado) {
		repository.save(grado);
	}

	@Override
	@Transactional
	public void delete(Integer grado_id) {
		repository.deleteById(grado_id);
	}

	@Override
	@Transactional(readOnly=true)
	public Grado findById(Integer grado_id) {
		return repository.findById(grado_id).orElse(null);
	}

	@Override
	@Transactional(readOnly=true)
	public Collection<Grado> findAll() {
		return (Collection<Grado>)repository.findAll();
	}
	
	
}
