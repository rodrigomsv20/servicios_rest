package com.olivos.mapper;

public class RegAsistenciaMapper {

	private Integer alumno_id;
	private Integer grado_id;
	private Integer curso_id;
	private String asistencias;
	private String fecha_registro;

	public RegAsistenciaMapper(Integer alumno_id, Integer grado_id, Integer curso_id, String asistencias,
			String fecha_registro) {
		this.alumno_id = alumno_id;
		this.grado_id = grado_id;
		this.curso_id = curso_id;
		this.asistencias = asistencias;
		this.fecha_registro = fecha_registro;
	}

	public Integer getAlumno_id() {
		return alumno_id;
	}

	public void setAlumno_id(Integer alumno_id) {
		this.alumno_id = alumno_id;
	}

	public Integer getGrado_id() {
		return grado_id;
	}

	public void setGrado_id(Integer grado_id) {
		this.grado_id = grado_id;
	}

	public Integer getCurso_id() {
		return curso_id;
	}

	public void setCurso_id(Integer curso_id) {
		this.curso_id = curso_id;
	}

	public String getAsistencias() {
		return asistencias;
	}

	public void setAsistencias(String asistencias) {
		this.asistencias = asistencias;
	}

	public String getFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(String fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

}
