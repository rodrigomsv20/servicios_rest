package com.olivos.service;

import java.util.Collection;

import com.olivos.model.Curso;

public interface CursoService {
	public abstract void insert(Curso curso);
	public abstract void update(Curso curso);
	public abstract void delete(Integer curso_id);
	public abstract Curso findById(Integer curso_id);
	public abstract Collection<Curso> findAll();
	
	public abstract Collection<Curso> listarCursoGrado(Integer grado_id);
}
