package com.olivos.service;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.olivos.model.Citacion;
import com.olivos.repository.CitacionRepository;

@Service
public class CitacionServiceImpl implements CitacionService{
	
	@Autowired
	private CitacionRepository repository;

	@Override
	@Transactional
	public void insert(Citacion citacion) {
		repository.save(citacion);
	}

	@Override
	@Transactional
	public void update(Citacion citacion) {
		repository.save(citacion);
	}

	@Override
	@Transactional
	public void delete(Integer citacion_id) {
		repository.deleteById(citacion_id);
	}

	@Override
	@Transactional(readOnly = true)
	public Citacion findById(Integer citacion_id) {
		return repository.findById(citacion_id).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public Collection<Citacion> findAll() {
		return (Collection<Citacion>)repository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Collection<Citacion> getCitacionDocente(Integer empleado_id, String fecha_ini, String fecha_fin) {
		return (Collection<Citacion>)repository.getCitacionDocente(empleado_id, fecha_ini, fecha_fin);
	}

	@Override
	@Transactional(readOnly=true)
	public Collection<Citacion> getCitDocentAlum(Integer empleado_id, Integer alumno_id, String fecha_ini,
			String fecha_fin) {
		return (Collection<Citacion>)repository.getCitDocentAlum(empleado_id, alumno_id, fecha_ini, fecha_fin);
	}

	@Override
	@Transactional(readOnly=true)
	public Collection<Citacion> citasAlumnoFecha(Integer alumno_id, Date fecha_cita) {
		return (Collection<Citacion>)repository.citasAlumnoFecha(alumno_id, fecha_cita);
	}
	
}
