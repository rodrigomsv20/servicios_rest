package com.olivos.service;

import java.util.Collection;

import com.olivos.model.Calificacion;

public interface CalificacionService {
	public abstract void insert(Calificacion calificacion);
	public abstract void update(Calificacion calificacion);
	public abstract void delete(Integer calificacion_id);
	public abstract Calificacion findById(Integer calificacion_id);
	public abstract Collection<Calificacion> findAll();
	
	public abstract Collection<Calificacion> listaCalifAlums(Integer grado_id, Integer curso_id,Integer periodoescolar_id);
	public abstract Calificacion califAlumno(Integer alumno_id,Integer curso_id,Integer periodoescolar_id);
	public abstract Collection<Calificacion> califAlumnoPerEscolar(Integer alumno_id, Integer periodoescolar_id);
}
