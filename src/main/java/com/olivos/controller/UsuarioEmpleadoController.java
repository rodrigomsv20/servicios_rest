package com.olivos.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.olivos.mapper.UsuarioEmpleadoMapper;
import com.olivos.model.UsuarioEmpleado;
import com.olivos.service.UsuarioEmpleadoService;
import com.olivos.util.MapperUtil;

@RestController
@RequestMapping("/usuarioempleado")
public class UsuarioEmpleadoController 
{
	
	@Autowired
	private UsuarioEmpleadoService usuarioEmpleadoService;
	
	@GetMapping("/listar")
	private ResponseEntity<?> listar() {
		Collection<UsuarioEmpleado> itemsUsuario = usuarioEmpleadoService.findAll();

		if (itemsUsuario.isEmpty()) {
			return new ResponseEntity<>("No se encontro lista de usuarios", HttpStatus.NO_CONTENT);
		}
		Collection<UsuarioEmpleadoMapper> itemsUsuarioMapper = MapperUtil.convertUsuaEmpMapper(itemsUsuario);
		return new ResponseEntity<>(itemsUsuarioMapper, HttpStatus.OK);
	}
	
	@PostMapping("/registrar")
	public ResponseEntity<?> registrar(@RequestBody UsuarioEmpleado usuario) {

		usuarioEmpleadoService.insert(usuario);
		return new ResponseEntity<>("¡Se creo el usuario con éxito!", HttpStatus.CREATED);
	}
	
	@PutMapping("/editar/{usuario_id}")
	public ResponseEntity<?> editar(@RequestBody UsuarioEmpleado editUsuarioEmp, @PathVariable Integer usuario_id) {

		UsuarioEmpleado usuarioEmpDB = usuarioEmpleadoService.findById(usuario_id);

		if (usuarioEmpDB != null) {
			usuarioEmpDB.setUsuario_empleado(editUsuarioEmp.getUsuario_empleado());
			usuarioEmpDB.setClave(editUsuarioEmp.getClave());
			usuarioEmpDB.setPerfilEmpleado(editUsuarioEmp.getPerfilEmpleado());
			usuarioEmpDB.setEmpleado(editUsuarioEmp.getEmpleado());
			
			return new ResponseEntity<>("¡Usuario actualizado!", HttpStatus.OK);
		}

		return new ResponseEntity<>("¡Usuario no encontrado!", HttpStatus.NOT_FOUND);
	}
}
