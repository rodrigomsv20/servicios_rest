package com.olivos.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "calificaciones")
public class Calificacion implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer calificacion_id;
	
	@Column(nullable = false)
	private Integer practica_calificada_1;
	
	@Column(nullable = false)
	private Integer practica_calificada_2;
	
	@Column(nullable = false)
	private Integer practica_calificada_3;

	@Column(nullable = false)
	private Integer examen_final;
	
	@Column(nullable = false)
	private Integer promedio;

	@ManyToOne
	@JoinColumn(name = "alumno_id", nullable = false, foreignKey = @ForeignKey(foreignKeyDefinition = "foreign key(alumno_id) references alumnos(alumno_id)"))
	private Alumno alumno;

	@ManyToOne
	@JoinColumn(name = "curso_id", nullable = false, foreignKey = @ForeignKey(foreignKeyDefinition = "foreign key(curso_id) references cursos(curso_id)"))
	private Curso curso;

	@ManyToOne
	@JoinColumn(name = "periodoescolar_id", nullable = false, foreignKey = @ForeignKey(foreignKeyDefinition = "foreign key(periodoescolar_id) references periodos_escolares(periodoescolar_id)"))
	private PeriodoEscolar periodoEscolar;
	
	public Calificacion() {}
	

	public Calificacion(Integer calificacion_id, Integer practica_calificada_1, Integer practica_calificada_2,
			Integer practica_calificada_3, Integer examen_final, Integer promedio) {
		this.calificacion_id = calificacion_id;
		this.practica_calificada_1 = practica_calificada_1;
		this.practica_calificada_2 = practica_calificada_2;
		this.practica_calificada_3 = practica_calificada_3;
		this.examen_final = examen_final;
		this.promedio = promedio;
	}

	public Integer getCalificacion_id() {
		return calificacion_id;
	}

	public void setCalificacion_id(Integer calificacion_id) {
		this.calificacion_id = calificacion_id;
	}

	public Integer getPractica_calificada_1() {
		return practica_calificada_1;
	}

	public void setPractica_calificada_1(Integer practica_calificada_1) {
		this.practica_calificada_1 = practica_calificada_1;
	}

	public Integer getPractica_calificada_2() {
		return practica_calificada_2;
	}

	public void setPractica_calificada_2(Integer practica_calificada_2) {
		this.practica_calificada_2 = practica_calificada_2;
	}

	public Integer getPractica_calificada_3() {
		return practica_calificada_3;
	}

	public void setPractica_calificada_3(Integer practica_calificada_3) {
		this.practica_calificada_3 = practica_calificada_3;
	}

	public Integer getExamen_final() {
		return examen_final;
	}

	public void setExamen_final(Integer examen_final) {
		this.examen_final = examen_final;
	}

	public Integer getPromedio() {
		return promedio;
	}

	public void setPromedio(Integer promedio) {
		this.promedio = promedio;
	}

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public PeriodoEscolar getPeriodoEscolar() {
		return periodoEscolar;
	}

	public void setPeriodoEscolar(PeriodoEscolar periodoEscolar) {
		this.periodoEscolar = periodoEscolar;
	}


	@Override
	public String toString() {
		return "Calificacion [calificacion_id=" + calificacion_id + ", practica_calificada_1=" + practica_calificada_1
				+ ", practica_calificada_2=" + practica_calificada_2 + ", practica_calificada_3="
				+ practica_calificada_3 + ", examen_final=" + examen_final + ", promedio=" + promedio + ", alumno="
				+ alumno + ", curso=" + curso + ", periodoEscolar=" + periodoEscolar + "]";
	}
	
	

}
