package com.olivos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.olivos.mapper.AlumnoMapper;
import com.olivos.mapper.EmpleadoMapper;
import com.olivos.model.Alumno;
import com.olivos.model.Empleado;
import com.olivos.model.Mensaje;
import com.olivos.model.UsuarioAlumno;
import com.olivos.model.UsuarioEmpleado;
import com.olivos.service.AlumnoService;
import com.olivos.service.EmpleadoService;
import com.olivos.service.UsuarioAlumnoService;
import com.olivos.service.UsuarioEmpleadoService;
import com.olivos.util.MapperUtil;

@RestController
@RequestMapping("/autentificar")
public class AutentificacionController {
	
	@Autowired
	private UsuarioAlumnoService usuarioAlumnoService;
	
	@Autowired
	private AlumnoService alumnoService;
	
	@Autowired
	private UsuarioEmpleadoService usuarioEmpleadoService;
	
	@Autowired
	private EmpleadoService empleadoService;
	
	
	
	@PostMapping("/Alumno")
	private ResponseEntity<?> autentificarAlumno(@RequestBody UsuarioAlumno ua){
		UsuarioAlumno usuarioAlum = usuarioAlumnoService.autentificarAlumno(ua.getUsuario_alumno(), ua.getClave());
		
		if(usuarioAlum == null) {
			Mensaje mensaje = new Mensaje("¡Usuario o contraseña incorrectos!");
			return new ResponseEntity<>(mensaje, HttpStatus.NOT_FOUND);
		}
		
		Alumno alumnoAut = alumnoService.findById(usuarioAlum.getAlumno().getAlumno_id());
		
		AlumnoMapper alumnoMapper = MapperUtil.convertAlumnoMapper(alumnoAut);
		return new ResponseEntity<>(alumnoMapper,HttpStatus.OK);
	}
	
	@PostMapping("/Empleado")
	private ResponseEntity<?> autentificarEmpleado(@RequestBody UsuarioEmpleado ue){
		UsuarioEmpleado usuarioEmp = usuarioEmpleadoService.autentificarEmpleado(ue.getUsuario_empleado(), ue.getClave());
		
		if(usuarioEmp == null) {
			Mensaje mensaje = new Mensaje("¡Usuario o contraseña incorrectos!");
			return new ResponseEntity<>(mensaje, HttpStatus.NO_CONTENT);
		}
		
		Empleado empleadoAut = empleadoService.findById(usuarioEmp.getEmpleado().getEmpleado_id());
		
		EmpleadoMapper empleadoMapper = MapperUtil.convertEmpleadoMapper(empleadoAut);
		return new ResponseEntity<>(empleadoMapper,HttpStatus.OK);
	}
	
}
