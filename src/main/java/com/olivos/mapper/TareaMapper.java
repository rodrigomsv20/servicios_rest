package com.olivos.mapper;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.olivos.model.Tarea;

public class TareaMapper {

	private Integer empleado_id;
	private Integer grado_id;
	private Integer curso_id;
	private String descripcion;
	private String fecha_registro;

	public TareaMapper(Tarea t) {
		this(t.getEmpleado().getEmpleado_id(), t.getGrado().getGrado_id(), t.getCurso().getCurso_id(),
				t.getDescripcion(), ConvertDateAString(t.getFecha_registro()));
	}
	
	public static String ConvertDateAString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String fechaComoCadena = sdf.format(date);
		System.out.println(fechaComoCadena);
		return fechaComoCadena;
	}

	public TareaMapper(Integer empleado_id, Integer grado_id, Integer curso_id, String descripcion,
			String fecha_registro) {
		this.empleado_id = empleado_id;
		this.grado_id = grado_id;
		this.curso_id = curso_id;
		this.descripcion = descripcion;
		this.fecha_registro = fecha_registro;
	}

	public Integer getEmpleado_id() {
		return empleado_id;
	}

	public void setEmpleado_id(Integer empleado_id) {
		this.empleado_id = empleado_id;
	}

	public Integer getGrado_id() {
		return grado_id;
	}

	public void setGrado_id(Integer grado_id) {
		this.grado_id = grado_id;
	}

	public Integer getCurso_id() {
		return curso_id;
	}

	public void setCurso_id(Integer curso_id) {
		this.curso_id = curso_id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(String fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

}
