package com.olivos.repository;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.olivos.model.Asistencia;

public interface AsistenciaRepository extends CrudRepository<Asistencia, Integer>{
	
	@Query(value = "{call usp_getListarAsisGraCurFecha(:grado_id,:curso_id,:fecha_registro)}", nativeQuery = true)
	public abstract Collection<Asistencia> listaAsistenciaAlumnos(@Param("grado_id") Integer grado_id, 
							@Param("curso_id") Integer curso_id, @Param("fecha_registro") Date fecha_registro);
	
	@Query(value = "{call usp_getAsistenciAlum(:alumno_id,:curso_id,:grado_id,:asistencias,:fecha_registro)}", nativeQuery = true)
	public abstract Asistencia asistenciAlum(@Param("alumno_id")Integer alumno_id,@Param("curso_id")Integer curso_id,
			@Param("grado_id")Integer grado_id,@Param("asistencias") String asistencias,@Param("fecha_registro") String fecha_registro);
	
	@Query(value = "{call usp_getAsisAlumnoFecha(:alumno_id,:fecha_registro)}", nativeQuery = true)
	public abstract Collection<Asistencia> asisAlumnoFecha(@Param("alumno_id")Integer alumno_id,@Param("fecha_registro") Date fecha_registro);
	
}	

