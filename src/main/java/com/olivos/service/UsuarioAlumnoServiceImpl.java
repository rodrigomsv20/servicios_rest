package com.olivos.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.olivos.model.UsuarioAlumno;
import com.olivos.repository.UsuarioAlumnoRepository;

@Service
public class UsuarioAlumnoServiceImpl implements UsuarioAlumnoService{
	
	@Autowired
	private UsuarioAlumnoRepository repository;
	
	@Override
	@Transactional
	public void insert(UsuarioAlumno usuarioAlum) {
		repository.save(usuarioAlum);
	}

	@Override
	@Transactional
	public void update(UsuarioAlumno usuarioAlum) {
		repository.save(usuarioAlum);
	}

	@Override
	@Transactional(readOnly=true)
	public UsuarioAlumno findById(Integer usuarioAlum_id) {
		return repository.findById(usuarioAlum_id).orElse(null);
	}

	@Override
	@Transactional(readOnly=true)
	public Collection<UsuarioAlumno> findAll() {
		return (Collection<UsuarioAlumno>)repository.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public UsuarioAlumno autentificarAlumno(String usuario, String clave) {
		UsuarioAlumno usuarioAlum = null;
		usuarioAlum = repository.autentificarAlumno(usuario, clave);
		return usuarioAlum;
	}


}
