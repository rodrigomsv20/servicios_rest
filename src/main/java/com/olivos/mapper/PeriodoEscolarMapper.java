package com.olivos.mapper;

import com.olivos.model.PeriodoEscolar;

public class PeriodoEscolarMapper {

	private Integer periodoescolar_id;
	private String nombre;

	public PeriodoEscolarMapper(PeriodoEscolar pe) {
		this(pe.getPeriodoescolar_id(), pe.getNombre());
	}

	public PeriodoEscolarMapper(Integer periodoescolar_id, String nombre) {
		this.periodoescolar_id = periodoescolar_id;
		this.nombre = nombre;
	}

	public Integer getPeriodoescolar_id() {
		return periodoescolar_id;
	}

	public void setPeriodoescolar_id(Integer periodoescolar_id) {
		this.periodoescolar_id = periodoescolar_id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
