package com.olivos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "alumnos")
public class Alumno implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer alumno_id;

	@Column(nullable = false, unique = true)
	private Integer dni_alumno;

	@Column(nullable = false)
	private String nombres;

	@Column(nullable = false)
	private String apellidos;

	@Column(nullable = false)
	private String fecha_nacimiento;

	@Column(nullable = false)
	private String direccion;

	@Temporal(TemporalType.DATE)
	private Date fecha_registro;

	@OneToOne(mappedBy = "alumno")
	private UsuarioAlumno usuarioAlumno;

	@OneToMany(mappedBy = "alumno")
	private Collection<Matricula> itemsMatricula = new ArrayList<>();

	@OneToMany(mappedBy = "alumno")
	private Collection<Asistencia> itemsAsistencias = new ArrayList<>();

	@OneToMany(mappedBy = "alumno")
	private Collection<Incidencia> itemsIncidencia = new ArrayList<>();

	@OneToMany(mappedBy = "alumno")
	private Collection<Citacion> itemsCitacion = new ArrayList<>();

	@OneToMany(mappedBy = "alumno")
	private Collection<Calificacion> itemsCalificacion = new ArrayList<>();

	public Alumno() {
	}

	public Alumno(Integer alumno_id) {
		this.alumno_id = alumno_id;
	}

	public Alumno(Integer alumno_id, Integer dni_alumno, String nombres, String apellidos, String fecha_nacimiento,
			String direccion, Date fecha_registro) {
		this.alumno_id = alumno_id;
		this.dni_alumno = dni_alumno;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.fecha_nacimiento = fecha_nacimiento;
		this.direccion = direccion;
		this.fecha_registro = fecha_registro;
	}

	@PostPersist
	public void postPersist() {
		fecha_registro = new Date();
	}

	public Integer getAlumno_id() {
		return alumno_id;
	}

	public void setAlumno_id(Integer alumno_id) {
		this.alumno_id = alumno_id;
	}

	public Integer getDni_alumno() {
		return dni_alumno;
	}

	public void setDni_alumno(Integer dni_alumno) {
		this.dni_alumno = dni_alumno;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(String fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Date getFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

	public UsuarioAlumno getUsuarioAlumno() {
		return usuarioAlumno;
	}

	public void setUsuarioAlumno(UsuarioAlumno usuarioAlumno) {
		this.usuarioAlumno = usuarioAlumno;
	}

	public Collection<Matricula> getItemsMatricula() {
		return itemsMatricula;
	}

	public void setItemsMatricula(Collection<Matricula> itemsMatricula) {
		this.itemsMatricula = itemsMatricula;
	}

	public Collection<Asistencia> getItemsAsistencias() {
		return itemsAsistencias;
	}

	public void setItemsAsistencias(Collection<Asistencia> itemsAsistencias) {
		this.itemsAsistencias = itemsAsistencias;
	}

	public Collection<Incidencia> getItemsIncidencia() {
		return itemsIncidencia;
	}

	public void setItemsIncidencia(Collection<Incidencia> itemsIncidencia) {
		this.itemsIncidencia = itemsIncidencia;
	}

	public Collection<Citacion> getItemsCitacion() {
		return itemsCitacion;
	}

	public void setItemsCitacion(Collection<Citacion> itemsCitacion) {
		this.itemsCitacion = itemsCitacion;
	}

	public Collection<Calificacion> getItemsCalificacion() {
		return itemsCalificacion;
	}

	public void setItemsCalificacion(Collection<Calificacion> itemsCalificacion) {
		this.itemsCalificacion = itemsCalificacion;
	}

}
