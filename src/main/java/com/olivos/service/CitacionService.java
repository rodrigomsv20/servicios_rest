package com.olivos.service;

import java.util.Collection;
import java.util.Date;

import com.olivos.model.Citacion;

public interface CitacionService {
	public abstract void insert(Citacion citacion);
	public abstract void update(Citacion citacion);
	public abstract void delete(Integer citacion_id);
	public abstract Citacion findById(Integer citacion_id);
	public abstract Collection<Citacion> findAll();
	
	public abstract Collection<Citacion> getCitacionDocente(Integer empleado_id, String fecha_ini,String fecha_fin);
	public abstract Collection<Citacion> getCitDocentAlum(Integer empleado_id, Integer alumno_id,String fecha_ini, String fecha_fin);
	public abstract Collection<Citacion> citasAlumnoFecha(Integer alumno_id, Date fecha_cita);
}
