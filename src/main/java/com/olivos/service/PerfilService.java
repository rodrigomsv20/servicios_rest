package com.olivos.service;

import java.util.Collection;

import com.olivos.model.Perfil;

public interface PerfilService
{
	public abstract void insert(Perfil perfil);
	public abstract void update(Perfil perfil);
	public abstract void delete(Integer perfil_id);
	public abstract Perfil findById(Integer perfil_id);
	public abstract Collection<Perfil> findAll();
}
