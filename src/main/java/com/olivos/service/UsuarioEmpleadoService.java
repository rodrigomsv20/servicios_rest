package com.olivos.service;

import java.util.Collection;

import com.olivos.model.UsuarioEmpleado;

public interface UsuarioEmpleadoService 
{
	public abstract void insert(UsuarioEmpleado usuarioEmp);
	public abstract void update(UsuarioEmpleado usuarioEmp);
	public abstract UsuarioEmpleado findById(Integer usuario_id);
	public abstract Collection<UsuarioEmpleado> findAll();
	public abstract UsuarioEmpleado autentificarEmpleado(String usuario, String clave);
}
