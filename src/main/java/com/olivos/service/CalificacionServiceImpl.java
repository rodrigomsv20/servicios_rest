package com.olivos.service;

import java.util.Collection;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.olivos.model.Calificacion;
import com.olivos.repository.CalificacionRepository;

@Service
public class CalificacionServiceImpl implements CalificacionService
{
	@Autowired
	private CalificacionRepository repository;
	
	@Override
	@Transactional
	public void insert(Calificacion calificacion) {
		repository.save(calificacion);
	}

	@Override
	@Transactional
	public void update(Calificacion calificacion) {
		repository.save(calificacion);
	}

	@Override
	@Transactional
	public void delete(Integer calificacion_id) {
		repository.deleteById(calificacion_id);
	}

	@Override
	@Transactional(readOnly=true)
	public Calificacion findById(Integer calificacion_id) {
		return repository.findById(calificacion_id).orElse(null);
	}

	@Override
	@Transactional(readOnly=true)
	public Collection<Calificacion> findAll() {
		return (Collection<Calificacion>)repository.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public Collection<Calificacion> listaCalifAlums(Integer grado_id, Integer curso_id, Integer periodoescolar_id) {
		return (Collection<Calificacion>)repository.listaCalifAlums(grado_id, curso_id, periodoescolar_id);
	}

	@Override
	@Transactional(readOnly=true)
	public Calificacion califAlumno(Integer alumno_id, Integer curso_id, Integer periodoescolar_id) {
		return repository.califAlumno(alumno_id, curso_id, periodoescolar_id);
	}

	@Override
	@Transactional(readOnly=true)
	public Collection<Calificacion> califAlumnoPerEscolar(Integer alumno_id, Integer periodoescolar_id) {
		return (Collection<Calificacion>)repository.califAlumnoPerEscolar(alumno_id, periodoescolar_id);
	}

}
