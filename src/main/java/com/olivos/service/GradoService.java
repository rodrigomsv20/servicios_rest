package com.olivos.service;

import java.util.Collection;

import com.olivos.model.Grado;

public interface GradoService {
	public abstract void insert(Grado grado);
	public abstract void update(Grado grado);
	public abstract void delete(Integer grado_id);
	public abstract Grado findById(Integer grado_id);
	public abstract Collection<Grado> findAll();
}
