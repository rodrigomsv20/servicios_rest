package com.olivos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "perfiles")
public class Perfil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer perfil_id;

	@Column(nullable = false, unique = true)
	private String tipo;

	// Relación de 1 a muchos Perfil - UsuarioEmpleado
	@OneToMany(mappedBy = "perfilEmpleado")
	private Collection<UsuarioEmpleado> itemsUsuarioEmpleado = new ArrayList<>();

	@OneToMany(mappedBy = "perfilAlumno")
	private Collection<UsuarioAlumno> itemsUsuarioAlumno = new ArrayList<>();

	public Perfil() {
	}

	public Perfil(Integer perfil_id, String tipo) {
		this.perfil_id = perfil_id;
		this.tipo = tipo;
	}

	public Integer getPerfil_id() {
		return perfil_id;
	}

	public void setPerfil_id(Integer perfil_id) {
		this.perfil_id = perfil_id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Collection<UsuarioEmpleado> getItemsUsuarioEmpleado() {
		return itemsUsuarioEmpleado;
	}

	public void setItemsUsuarioEmpleado(Collection<UsuarioEmpleado> itemsUsuarioEmpleado) {
		this.itemsUsuarioEmpleado = itemsUsuarioEmpleado;
	}

	public Collection<UsuarioAlumno> getItemsUsuarioAlumno() {
		return itemsUsuarioAlumno;
	}

	public void setItemsUsuarioAlumno(Collection<UsuarioAlumno> itemsUsuarioAlumno) {
		this.itemsUsuarioAlumno = itemsUsuarioAlumno;
	}

}
