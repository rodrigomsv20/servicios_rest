package com.olivos.controller;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.olivos.mapper.AsistenciaMapper;
import com.olivos.mapper.RegAsistenciaMapper;
import com.olivos.mapper.VerAsistenciaMapper;
import com.olivos.model.Alumno;
import com.olivos.model.Asistencia;
import com.olivos.model.Curso;
import com.olivos.model.Grado;
import com.olivos.model.Mensaje;
import com.olivos.service.AsistenciaService;
import com.olivos.util.MapperUtil;

@RestController
@RequestMapping("/asistencia")
public class AsistenciaController {
	
	@Autowired
	private AsistenciaService asistenciaService;
	
	@GetMapping("/listar")
	private ResponseEntity<?> listar() {
		Collection<Asistencia> itemsAsistencia = asistenciaService.findAll();

		if (itemsAsistencia.isEmpty()) {
			return new ResponseEntity<>("No se encontro lista de asistencias", HttpStatus.NO_CONTENT);
		}
		Collection<AsistenciaMapper> itemsAsistenciaMapper = MapperUtil.convertAsistMapper(itemsAsistencia);
		return new ResponseEntity<>(itemsAsistenciaMapper, HttpStatus.OK);
	}

	@PostMapping("/registrar")
	public ResponseEntity<?> registrar(@RequestBody RegAsistenciaMapper asistenciaMapp) {
		
		Asistencia asistencia = new Asistencia();
		Alumno alumno = new Alumno(asistenciaMapp.getAlumno_id());
		Curso curso = new Curso(asistenciaMapp.getCurso_id());
		Grado grado = new Grado(asistenciaMapp.getGrado_id());
		
		asistencia.setAsistencias((asistenciaMapp.getAsistencias().equals("Asistio")?"Asistió":"No Asistió"));
		asistencia.setAlumno(alumno);
		asistencia.setCurso(curso);
		asistencia.setGrado(grado);
		
		asistenciaService.insert(asistencia);
		return new ResponseEntity<>("¡Se creo la asistencia con éxito!", HttpStatus.CREATED);
	}
	
	@PutMapping("/editar/{asistencia_id}")
	public ResponseEntity<?> editar(@RequestBody Asistencia editAsistencia, @PathVariable Integer asistencia_id) {

		Asistencia asistenciaDB = asistenciaService.findById(asistencia_id);

		if (asistenciaDB != null) {
			asistenciaDB.setAsistencias(editAsistencia.getAsistencias());
			
			return new ResponseEntity<>("¡Asistencia actualizado!", HttpStatus.OK);
		}

		return new ResponseEntity<>("¡Asistencia no encontrado!", HttpStatus.NOT_FOUND);
	}
	
	@DeleteMapping("/borrar/{asistencia_id}")
	public ResponseEntity<?> borrar(@PathVariable Integer asistencia_id) {
		Asistencia asistenciaDB = asistenciaService.findById(asistencia_id);

		if (asistenciaDB != null) {
			asistenciaService.delete(asistencia_id);
			return new ResponseEntity<>("¡Asistencia eliminado!", HttpStatus.OK);
		}
		return new ResponseEntity<>("¡Asistencia no encontrado!", HttpStatus.NOT_FOUND);
	}
	
	
	//Servicios personalizados
	@GetMapping("/listarAlumGradoCursoFecha/{grado_id}/{curso_id}/{fecha_registro}")
	private ResponseEntity<?> listarAlumGradoCursoFecha(@PathVariable Integer grado_id,@PathVariable Integer curso_id,
														@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd")  Date fecha_registro) {
		
		Collection<Asistencia> itemsAsistencia = asistenciaService.listaAsistenciaAlumnos(grado_id, curso_id, fecha_registro);

		if (itemsAsistencia.isEmpty()) {
			return new ResponseEntity<>("No se encontro lista de asistencias", HttpStatus.NO_CONTENT);
		}
		Collection<AsistenciaMapper> itemsAsistenciaMapper = MapperUtil.convertAsistMapper(itemsAsistencia);
		return new ResponseEntity<>(itemsAsistenciaMapper, HttpStatus.OK);
	}
	
	@PutMapping("/editarAsistenciaAlum")
	public ResponseEntity<?> editarAsistenciaAlum(@RequestBody RegAsistenciaMapper asistenciaMapp) {
		
		Asistencia asistenciaDB = asistenciaService.asistenciAlum(asistenciaMapp.getAlumno_id(), asistenciaMapp.getCurso_id(),
				asistenciaMapp.getGrado_id(), asistenciaMapp.getAsistencias(),asistenciaMapp.getFecha_registro());
		
		if (asistenciaDB != null) {
			asistenciaDB.setAsistencias(asistenciaMapp.getAsistencias());
			
			asistenciaService.update(asistenciaDB);
			return new ResponseEntity<>("¡Asistencia actualizado!", HttpStatus.OK);
		}

		return new ResponseEntity<>("¡Asistencia no encontrado!", HttpStatus.NOT_FOUND);
	}
	
	@GetMapping("/listaAsisAlumnoFecha/{alumno_id}/{fecha_registro}")
	private ResponseEntity<?> listaAsisAlumnoFecha(@PathVariable Integer alumno_id,
			@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Date fecha_registro) {

		Collection<Asistencia> itemsAsistencia = asistenciaService.asisAlumnoFecha(alumno_id, fecha_registro);

		if (itemsAsistencia.isEmpty()) {
			Mensaje mensaje = new Mensaje("¡No tiene asistencias registradas!");
			return new ResponseEntity<>(mensaje, HttpStatus.NOT_FOUND);
		}

		Collection<VerAsistenciaMapper> itemsVerAsistenciaMapper = MapperUtil.convertVerAsistenciaMapper(itemsAsistencia);
		return new ResponseEntity<>(itemsVerAsistenciaMapper, HttpStatus.OK);
	}
		
}
