package com.olivos.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.olivos.mapper.PerfilMapper;
import com.olivos.model.Perfil;
import com.olivos.service.PerfilService;
import com.olivos.util.MapperUtil;

@RestController
@RequestMapping("/perfil")
public class PerfilController {
	
	@Autowired
	private PerfilService perfilService;
	
	@GetMapping("/listar")
	private ResponseEntity<?> listar() {
		Collection<Perfil> itemsPerfil = perfilService.findAll();

		if (itemsPerfil.isEmpty()) {
			return new ResponseEntity<>("No se encontro lista de perfiles", HttpStatus.NO_CONTENT);
		}
		Collection<PerfilMapper> itemsPerfilMapper = MapperUtil.convertPerfMapper(itemsPerfil);
		return new ResponseEntity<>(itemsPerfilMapper, HttpStatus.OK);
	}
	
	@PostMapping("/registrar")
	public ResponseEntity<?> registrar(@RequestBody Perfil perfil) {

		perfilService.insert( perfil);
		return new ResponseEntity<>("¡Se creo el perfil con éxito!", HttpStatus.CREATED);
	}
	
	@PutMapping("/editar/{perfil_id}")
	public ResponseEntity<?> editar(@RequestBody Perfil editPerfil, @PathVariable Integer perfil_id) {

		Perfil PerfilDB = perfilService.findById(perfil_id);

		if (PerfilDB != null) {
			PerfilDB.setTipo(editPerfil.getTipo());

			return new ResponseEntity<>("¡Perfil actualizado!", HttpStatus.OK);
		}

		return new ResponseEntity<>("¡Perfil no encontrado!", HttpStatus.NOT_FOUND);
	}
	
	@DeleteMapping("/borrar/{perfil_id}")
	public ResponseEntity<?> borrar(@PathVariable Integer perfil_id) {
		Perfil PerfilDB = perfilService.findById(perfil_id);
		
		if (PerfilDB != null) {
			perfilService.delete(perfil_id);
			return new ResponseEntity<>("¡Perfil eliminado!", HttpStatus.OK);
		}
		return new ResponseEntity<>("¡Perfil no encontrado!", HttpStatus.NOT_FOUND);
	}
	
}
