package com.olivos.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.olivos.model.UsuarioAlumno;

public interface UsuarioAlumnoRepository extends CrudRepository<UsuarioAlumno, Integer>{
	
	@Query(value = "{call usp_getAutentificarAlumno(:usuario,:clave)}", nativeQuery = true)
	public abstract UsuarioAlumno autentificarAlumno(@Param("usuario") String usuario, @Param("clave") String clave);
	
}
