package com.olivos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "empleados")
public class Empleado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer empleado_id;

	@Column(nullable = false, unique = true)
	private Integer dni_empleado;

	@Column(nullable = false)
	private String nombres;

	@Column(nullable = false)
	private String apellido;

	@Column(nullable = false)
	private String fecha_nacimiento;

	@Column(nullable = false)
	private String sexo;

	@Column(nullable = false)
	private Integer telefono;

	@Column(nullable = false, unique = true)
	private String correo;

	@Column(nullable = false)
	private String profesion;

	@Column(nullable = false)
	private Double sueldo;

	@Temporal(TemporalType.DATE)
	private Date fecha_registro;

	@OneToOne(mappedBy = "empleado")
	private UsuarioEmpleado usuarioEmpleado;

	@OneToMany(mappedBy = "empleado")
	private Collection<Tarea> itemsTarea = new ArrayList<>();

	@OneToMany(mappedBy = "empleado")
	private Collection<Incidencia> itemsIncidencia = new ArrayList<>();

	@OneToMany(mappedBy = "empleado")
	private Collection<Citacion> itemsCitacion = new ArrayList<>();

	public Empleado() {
	}

	public Empleado(Integer empleado_id) {
		this.empleado_id = empleado_id;
	}

	public Empleado(Integer empleado_id, Integer dni_empleado, String nombres, String apellido, String fecha_nacimiento,
			String sexo, Integer telefono, String correo, String profesion, Double sueldo, Date fecha_registro) {
		this.empleado_id = empleado_id;
		this.dni_empleado = dni_empleado;
		this.nombres = nombres;
		this.apellido = apellido;
		this.fecha_nacimiento = fecha_nacimiento;
		this.sexo = sexo;
		this.telefono = telefono;
		this.correo = correo;
		this.profesion = profesion;
		this.sueldo = sueldo;
		this.fecha_registro = fecha_registro;
	}

	@PostPersist
	public void postPersist() {
		fecha_registro = new Date();
	}

	public Integer getEmpleado_id() {
		return empleado_id;
	}

	public void setEmpleado_id(Integer empleado_id) {
		this.empleado_id = empleado_id;
	}

	public Integer getDni_empleado() {
		return dni_empleado;
	}

	public void setDni_empleado(Integer dni_empleado) {
		this.dni_empleado = dni_empleado;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(String fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public Integer getTelefono() {
		return telefono;
	}

	public void setTelefono(Integer telefono) {
		this.telefono = telefono;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getProfesion() {
		return profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	public Double getSueldo() {
		return sueldo;
	}

	public void setSueldo(Double sueldo) {
		this.sueldo = sueldo;
	}

	public Date getFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

	public UsuarioEmpleado getUsuarioEmpleado() {
		return usuarioEmpleado;
	}

	public void setUsuarioEmpleado(UsuarioEmpleado usuario) {
		this.usuarioEmpleado = usuario;
	}

	public Collection<Tarea> getItemsTarea() {
		return itemsTarea;
	}

	public void setItemsTarea(Collection<Tarea> itemsTarea) {
		this.itemsTarea = itemsTarea;
	}

	public Collection<Incidencia> getItemsIncidencia() {
		return itemsIncidencia;
	}

	public void setItemsIncidencia(Collection<Incidencia> itemsIncidencia) {
		this.itemsIncidencia = itemsIncidencia;
	}

	public Collection<Citacion> getItemsCitacion() {
		return itemsCitacion;
	}

	public void setItemsCitacion(Collection<Citacion> itemsCitacion) {
		this.itemsCitacion = itemsCitacion;
	}

}
