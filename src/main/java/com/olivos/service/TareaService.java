package com.olivos.service;

import java.util.Collection;
import java.util.Date;

import com.olivos.model.Tarea;

public interface TareaService {
	public abstract void insert(Tarea tarea);
	public abstract void update(Tarea tarea);
	public abstract void delete(Integer tarea_id);
	public abstract Tarea findById(Integer tarea_id);
	public abstract Collection<Tarea> findAll();
	
	public abstract Tarea tarea(Integer grado_id,Integer curso_id,Date fecha_registro);
	public abstract Tarea tareaAlum(Integer empleado_id,Integer curso_id,Integer grado_id, String descripcion,String fecha_registro);
	public abstract Collection<Tarea> tareasGradoFecha(Integer alumno_id,Date fecha_registro);
}
