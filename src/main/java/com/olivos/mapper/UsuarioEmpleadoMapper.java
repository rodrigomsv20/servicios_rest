package com.olivos.mapper;

import com.olivos.model.UsuarioEmpleado;

public class UsuarioEmpleadoMapper {

	private Integer usuario_id;
	private String usuario_empleado;
	private String clave;
	private String estado;
	private Integer perfil_id;
	private Integer empleado_id;

	public UsuarioEmpleadoMapper(UsuarioEmpleado ue) {
		this(ue.getUsuario_id(),ue.getUsuario_empleado(), ue.getClave(), ue.getEstado(), ue.getPerfilEmpleado().getPerfil_id(),
				ue.getEmpleado().getEmpleado_id());
	}

	public UsuarioEmpleadoMapper(Integer usuario_id, String usuario_empleado, String clave, String estado,
			Integer perfil_id, Integer empleado_id) {
		this.usuario_id = usuario_id;
		this.usuario_empleado = usuario_empleado;
		this.clave = clave;
		this.estado = estado;
		this.perfil_id = perfil_id;
		this.empleado_id = empleado_id;
	}

	public Integer getUsuario_id() {
		return usuario_id;
	}

	public void setUsuario_id(Integer usuario_id) {
		this.usuario_id = usuario_id;
	}

	public String getUsuario_empleado() {
		return usuario_empleado;
	}

	public void setUsuario_empleado(String usuario_empleado) {
		this.usuario_empleado = usuario_empleado;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Integer getPerfil_id() {
		return perfil_id;
	}

	public void setPerfil_id(Integer perfil_id) {
		this.perfil_id = perfil_id;
	}

	public Integer getEmpleado_id() {
		return empleado_id;
	}

	public void setEmpleado_id(Integer empleado_id) {
		this.empleado_id = empleado_id;
	}

}
