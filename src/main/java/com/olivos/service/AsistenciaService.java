package com.olivos.service;

import java.util.Collection;
import java.util.Date;

import com.olivos.model.Asistencia;

public interface AsistenciaService {
	public abstract void insert(Asistencia asistencia);
	public abstract void update(Asistencia asistencia);
	public abstract void delete(Integer asistencia_id);
	public abstract Asistencia findById(Integer asistencia_id);
	public abstract Collection<Asistencia> findAll();
	
	public abstract Collection<Asistencia> listaAsistenciaAlumnos(Integer grado_id, Integer curso_id, Date fecha_registro);
	public abstract Asistencia asistenciAlum(Integer alumno_id,Integer curso_id,Integer grado_id,String asistencias,String fecha_registro);
	public abstract Collection<Asistencia> asisAlumnoFecha(Integer alumno_id, Date fecha_registro);
}
