package com.olivos.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "usuarios_alumnos")
public class UsuarioAlumno implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer usuario_id;

	@Column(nullable = false, unique = true)
	private String usuario_alumno;

	@Column
	private String clave;

	@Column
	private String estado;

	@OneToOne
	@JoinColumn(name = "alumno_id", nullable = false, unique = true, foreignKey = @ForeignKey(foreignKeyDefinition = "foreign key(alumno_id) references alumnos(alumno_id)"))
	private Alumno alumno;

	// Relación muchos a 1 entre Perfil - UsuarioAlumno
	@ManyToOne
	@JoinColumn(name = "perfil_id", nullable = false, foreignKey = @ForeignKey(foreignKeyDefinition = "foreign key(perfil_id) references perfiles(perfil_id)"))
	private Perfil perfilAlumno;

	public UsuarioAlumno() {
	}

	public UsuarioAlumno(Integer usuario_id, String usuario_alumno, String clave, String estado) {
		this.usuario_id = usuario_id;
		this.usuario_alumno = usuario_alumno;
		this.clave = clave;
		this.estado = estado;
	}

	public Integer getUsuario_id() {
		return usuario_id;
	}

	public void setUsuario_id(Integer usuario_id) {
		this.usuario_id = usuario_id;
	}

	public String getUsuario_alumno() {
		return usuario_alumno;
	}

	public void setUsuario_alumno(String usuario_alumno) {
		this.usuario_alumno = usuario_alumno;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public Perfil getPerfilAlumno() {
		return perfilAlumno;
	}

	public void setPerfilAlumno(Perfil perfilAlumno) {
		this.perfilAlumno = perfilAlumno;
	}

}
