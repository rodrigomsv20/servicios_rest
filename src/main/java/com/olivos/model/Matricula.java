package com.olivos.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "matriculas")
public class Matricula implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer matricula_id;

	@Column(nullable = false, unique = true)
	private Integer dni_apoderado;

	@Column(nullable = false)
	private String nombres;

	@Column(nullable = false)
	private String apellidos;

	@Column(nullable = false)
	private String parentes;

	@Column(nullable = false)
	private Integer telefono;

	@Column(nullable = false)
	private String correo;

	@ManyToOne
	@JoinColumn(name = "grado_id", nullable = false, foreignKey = @ForeignKey(foreignKeyDefinition = "foreign key(grado_id) references grados(grado_id)"))
	private Grado grado;

	@ManyToOne
	@JoinColumn(name = "alumno_id", nullable = false, unique = true, foreignKey = @ForeignKey(foreignKeyDefinition = "foreign key(alumno_id) references alumnos(alumno_id)"))
	private Alumno alumno;

	public Matricula() {
	}

	public Matricula(Integer matricula_id, Integer dni_apoderado, String nombres, String apellidos, String parentes,
			Integer telefono, String correo) {
		super();
		this.matricula_id = matricula_id;
		this.dni_apoderado = dni_apoderado;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.parentes = parentes;
		this.telefono = telefono;
		this.correo = correo;
	}

	public Integer getMatricula_id() {
		return matricula_id;
	}

	public void setMatricula_id(Integer matricula_id) {
		this.matricula_id = matricula_id;
	}

	public Integer getDni_apoderado() {
		return dni_apoderado;
	}

	public void setDni_apoderado(Integer dni_apoderado) {
		this.dni_apoderado = dni_apoderado;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getParentes() {
		return parentes;
	}

	public void setParentes(String parentes) {
		this.parentes = parentes;
	}

	public Integer getTelefono() {
		return telefono;
	}

	public void setTelefono(Integer telefono) {
		this.telefono = telefono;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public Grado getGrado() {
		return grado;
	}

	public void setGrado(Grado grado) {
		this.grado = grado;
	}

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

}
