package com.olivos.mapper;

import java.util.Date;

import com.olivos.model.Citacion;

public class VerCitacionMapper {
	private Integer citacion_id;
	private String fecha_cita;
	private Date fecha_registro;
	private String motivo;
	private Integer alumno_id;
	private Integer empleado_id;
	private String nombres;
	private String apellidos;

	public VerCitacionMapper(Citacion c) {
		this(c.getCitacion_id(), c.getFecha_cita(), c.getFecha_registro(), c.getMotivo(), c.getAlumno().getAlumno_id(),
				c.getEmpleado().getEmpleado_id(), c.getEmpleado().getNombres(),c.getEmpleado().getApellido());
	}

	public VerCitacionMapper(Integer citacion_id, String fecha_cita, Date fecha_registro, String motivo,
			Integer alumno_id, Integer empleado_id, String nombres, String apellidos) {
		super();
		this.citacion_id = citacion_id;
		this.fecha_cita = fecha_cita;
		this.fecha_registro = fecha_registro;
		this.motivo = motivo;
		this.alumno_id = alumno_id;
		this.empleado_id = empleado_id;
		this.nombres = nombres;
		this.apellidos = apellidos;
	}

	public Integer getCitacion_id() {
		return citacion_id;
	}

	public void setCitacion_id(Integer citacion_id) {
		this.citacion_id = citacion_id;
	}

	public String getFecha_cita() {
		return fecha_cita;
	}

	public void setFecha_cita(String fecha_cita) {
		this.fecha_cita = fecha_cita;
	}

	public Date getFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public Integer getAlumno_id() {
		return alumno_id;
	}

	public void setAlumno_id(Integer alumno_id) {
		this.alumno_id = alumno_id;
	}

	public Integer getEmpleado_id() {
		return empleado_id;
	}

	public void setEmpleado_id(Integer empleado_id) {
		this.empleado_id = empleado_id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

}
