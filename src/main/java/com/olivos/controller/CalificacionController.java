package com.olivos.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.olivos.mapper.CalificacionMapper;
import com.olivos.mapper.RegCalificacionMapper;
import com.olivos.mapper.VerCalificacionMapper;
import com.olivos.model.Alumno;
import com.olivos.model.Calificacion;
import com.olivos.model.Curso;
import com.olivos.model.Mensaje;
import com.olivos.model.PeriodoEscolar;
import com.olivos.service.CalificacionService;
import com.olivos.util.MapperUtil;

@RestController
@RequestMapping("/calificacion")
public class CalificacionController {

	@Autowired
	private CalificacionService calificacionService;

	@GetMapping("/listarCalifAlumnos/{grado_id}/{curso_id}/{periodoescolar_id}")
	private ResponseEntity<?> listarCalifAlumnos(@PathVariable Integer grado_id, @PathVariable Integer curso_id,
			@PathVariable Integer periodoescolar_id) {
		Collection<Calificacion> itemsCalificacion = calificacionService.listaCalifAlums(grado_id, curso_id,
				periodoescolar_id);

		if (itemsCalificacion.isEmpty()) {
			return new ResponseEntity<>("No se encontro lista de calificaciones", HttpStatus.NO_CONTENT);
		}
		Collection<CalificacionMapper> itemsCalificacionMapper = MapperUtil
				.convertCalificacionMapper(itemsCalificacion);
		return new ResponseEntity<>(itemsCalificacionMapper, HttpStatus.OK);
	}

	@PostMapping
	private ResponseEntity<?> registrar(@RequestBody RegCalificacionMapper regCalificacionMapper) {
		Calificacion calificacion = new Calificacion();

		Alumno alumno = new Alumno(regCalificacionMapper.getAlumno_id());

		Curso curso = new Curso(regCalificacionMapper.getCurso_id());

		PeriodoEscolar periodoEscolar = new PeriodoEscolar(regCalificacionMapper.getPeriodoescolar_id());

		calificacion.setPractica_calificada_1(regCalificacionMapper.getPractica_calificada_1());
		calificacion.setPractica_calificada_2(regCalificacionMapper.getPractica_calificada_2());
		calificacion.setPractica_calificada_3(regCalificacionMapper.getPractica_calificada_3());

		calificacion.setExamen_final(regCalificacionMapper.getExamen_final());
		calificacion.setPromedio(regCalificacionMapper.getPromedio());

		calificacion.setAlumno(alumno);
		calificacion.setCurso(curso);
		calificacion.setPeriodoEscolar(periodoEscolar);

		calificacionService.insert(calificacion);

		return new ResponseEntity<>("Se creo la calificacion con exito", HttpStatus.CREATED);
	}

	@PutMapping("/editar")
	public ResponseEntity<?> editar(@RequestBody RegCalificacionMapper regCalificacionMapper) {

		Calificacion calificacionDB = calificacionService.califAlumno(regCalificacionMapper.getAlumno_id(),
				regCalificacionMapper.getCurso_id(), regCalificacionMapper.getPeriodoescolar_id());
		
		if (calificacionDB != null) {

			Alumno alumno = new Alumno(regCalificacionMapper.getAlumno_id());
			Curso curso = new Curso(regCalificacionMapper.getCurso_id());
			PeriodoEscolar periodoEscolar = new PeriodoEscolar(regCalificacionMapper.getPeriodoescolar_id());

			calificacionDB.setPractica_calificada_1(regCalificacionMapper.getPractica_calificada_1());
			calificacionDB.setPractica_calificada_2(regCalificacionMapper.getPractica_calificada_2());
			calificacionDB.setPractica_calificada_3(regCalificacionMapper.getPractica_calificada_3());
			calificacionDB.setExamen_final(regCalificacionMapper.getExamen_final());
			calificacionDB.setPromedio(calcularPromedio(regCalificacionMapper.getPractica_calificada_1(),
					regCalificacionMapper.getPractica_calificada_2(), regCalificacionMapper.getPractica_calificada_3(),
					regCalificacionMapper.getExamen_final()));
			calificacionDB.setAlumno(alumno);
			calificacionDB.setCurso(curso);
			calificacionDB.setPeriodoEscolar(periodoEscolar);

			calificacionService.update(calificacionDB);
			return new ResponseEntity<>("¡Calificación actualizado!", HttpStatus.OK);
		}

		return new ResponseEntity<>("¡Calificación no encontrado!", HttpStatus.NOT_FOUND);
	}

	public Integer calcularPromedio(Integer practica_calificada_1, Integer practica_calificada_2,
			Integer practica_calificada_3, Integer examen_final) {
		Integer promedio = 0;
		Integer promedioPC = 0;
		if (practica_calificada_1 > 0 && practica_calificada_2 > 0 && practica_calificada_3 > 0 && examen_final > 0) {
			promedioPC = (practica_calificada_1 + practica_calificada_2 + practica_calificada_3) / 3;
			promedio = (promedioPC + examen_final) / 2;
		}
		return promedio;
	}

	@GetMapping("/listaCalifAlumnoPerEscolar/{alumno_id}/{periodoescolar_id}")
	private ResponseEntity<?> listaCalifAlumnoPerEscolar(@PathVariable Integer alumno_id,@PathVariable Integer periodoescolar_id) {

		Collection<Calificacion> itemsCalificacion = calificacionService.califAlumnoPerEscolar(alumno_id, periodoescolar_id);

		if (itemsCalificacion.isEmpty()) {
			Mensaje mensaje = new Mensaje("¡No se encontro calificaciones para este trimestre!");
			return new ResponseEntity<>(mensaje, HttpStatus.NOT_FOUND);
		}

		Collection<VerCalificacionMapper> itemsVerCalificacionMapper = MapperUtil.convertVerCalificacionMapper(itemsCalificacion);
		return new ResponseEntity<>(itemsVerCalificacionMapper, HttpStatus.OK);
	}

}
