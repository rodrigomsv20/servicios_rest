package com.olivos.mapper;

public class RegCitacionMapper {

	private Integer citacion_id;
	private String fecha_cita;
	private String motivo;
	private Integer alumno_id;
	private Integer empleado_id;

	public RegCitacionMapper(Integer citacion_id, String fecha_cita, String motivo, Integer alumno_id,
			Integer empleado_id) {
		this.citacion_id = citacion_id;
		this.fecha_cita = fecha_cita;
		this.motivo = motivo;
		this.alumno_id = alumno_id;
		this.empleado_id = empleado_id;
	}

	public Integer getCitacion_id() {
		return citacion_id;
	}

	public void setCitacion_id(Integer citacion_id) {
		this.citacion_id = citacion_id;
	}

	public String getFecha_cita() {
		return fecha_cita;
	}

	public void setFecha_cita(String fecha_cita) {
		this.fecha_cita = fecha_cita;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public Integer getAlumno_id() {
		return alumno_id;
	}

	public void setAlumno_id(Integer alumno_id) {
		this.alumno_id = alumno_id;
	}

	public Integer getEmpleado_id() {
		return empleado_id;
	}

	public void setEmpleado_id(Integer empleado_id) {
		this.empleado_id = empleado_id;
	}

}
