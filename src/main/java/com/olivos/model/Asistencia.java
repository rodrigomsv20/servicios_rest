package com.olivos.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "asistencias")
public class Asistencia implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer asistencia_id;
	
	@Column(nullable = false)
	private String asistencias;

	@Temporal(TemporalType.DATE)
	private Date fecha_registro;
	
	@ManyToOne
	@JoinColumn(name = "alumno_id", nullable = false, foreignKey = @ForeignKey(foreignKeyDefinition = "foreign key(alumno_id) references alumnos(alumno_id)"))
	private Alumno alumno;
	
	@ManyToOne
	@JoinColumn(name = "grado_id", nullable = false, foreignKey = @ForeignKey(foreignKeyDefinition = "foreign key(grado_id) references grados(grado_id)"))
	private Grado grado;
	
	@ManyToOne
	@JoinColumn(name = "curso_id", nullable = false, foreignKey = @ForeignKey(foreignKeyDefinition = "foreign key(curso_id) references cursos(curso_id)"))
	private Curso curso;

	public Asistencia() {
	}

	public Asistencia(String asistencias, Date fecha_registro) {
		this.asistencias = asistencias;
		this.fecha_registro = fecha_registro;
	}
	
	@PostPersist
	public void postPersist() {
		fecha_registro = new Date();
	}

	public String getAsistencias() {
		return asistencias;
	}

	public void setAsistencias(String asistencias) {
		this.asistencias = asistencias;
	}

	public Date getFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

	public Integer getAsistencia_id() {
		return asistencia_id;
	}

	public void setAsistencia_id(Integer asistencia_id) {
		this.asistencia_id = asistencia_id;
	}

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public Grado getGrado() {
		return grado;
	}

	public void setGrado(Grado grado) {
		this.grado = grado;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}
	
	
}
