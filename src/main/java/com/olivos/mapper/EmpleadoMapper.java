package com.olivos.mapper;

import java.util.Date;

import com.olivos.model.Empleado;

public class EmpleadoMapper {

	private Integer empleado_id;
	private Integer dni_empleado;
	private String nombres;
	private String apellido;
	private String fecha_nacimiento;
	private String sexo;
	private Integer telefono;
	private String correo;
	private String profesion;
	private Double sueldo;
	private Date fecha_registro;
	private String usuario_empleado;
	private String estado;

	public EmpleadoMapper(Empleado e) {
		this(e.getEmpleado_id(),e.getDni_empleado(),e.getNombres(),e.getApellido(),e.getFecha_nacimiento(),e.getSexo(),
				e.getTelefono(),e.getCorreo(),e.getProfesion(),e.getSueldo(),e.getFecha_registro(),
				e.getUsuarioEmpleado().getUsuario_empleado(),e.getUsuarioEmpleado().getEstado());
	}
	
	
	public EmpleadoMapper(Integer empleado_id, Integer dni_empleado, String nombres, String apellido,
			String fecha_nacimiento, String sexo, Integer telefono, String correo, String profesion, Double sueldo,
			Date fecha_registro, String usuario_empleado, String estado) {
		this.empleado_id = empleado_id;
		this.dni_empleado = dni_empleado;
		this.nombres = nombres;
		this.apellido = apellido;
		this.fecha_nacimiento = fecha_nacimiento;
		this.sexo = sexo;
		this.telefono = telefono;
		this.correo = correo;
		this.profesion = profesion;
		this.sueldo = sueldo;
		this.fecha_registro = fecha_registro;
		this.usuario_empleado = usuario_empleado;
		this.estado = estado;
	}

	public Integer getEmpleado_id() {
		return empleado_id;
	}

	public void setEmpleado_id(Integer empleado_id) {
		this.empleado_id = empleado_id;
	}

	public Integer getDni_empleado() {
		return dni_empleado;
	}

	public void setDni_empleado(Integer dni_empleado) {
		this.dni_empleado = dni_empleado;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(String fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public Integer getTelefono() {
		return telefono;
	}

	public void setTelefono(Integer telefono) {
		this.telefono = telefono;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getProfesion() {
		return profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	public Double getSueldo() {
		return sueldo;
	}

	public void setSueldo(Double sueldo) {
		this.sueldo = sueldo;
	}

	public Date getFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

	public String getUsuario_empleado() {
		return usuario_empleado;
	}

	public void setUsuario_empleado(String usuario_empleado) {
		this.usuario_empleado = usuario_empleado;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

}
