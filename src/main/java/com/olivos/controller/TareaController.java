package com.olivos.controller;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.olivos.mapper.TareaMapper;
import com.olivos.mapper.VerTareaMapper;
import com.olivos.model.Curso;
import com.olivos.model.Empleado;
import com.olivos.model.Grado;
import com.olivos.model.Mensaje;
import com.olivos.model.Tarea;
import com.olivos.service.TareaService;
import com.olivos.util.MapperUtil;

@RestController
@RequestMapping("/tarea")
public class TareaController {
	
	@Autowired
	private TareaService tareaService;
	@GetMapping("/listar")
	private ResponseEntity<?> listar() {
		Collection<Tarea> itemsTarea = tareaService.findAll();

		if (itemsTarea.isEmpty()) {
			return new ResponseEntity<>("No se encontro lista de tareas", HttpStatus.NO_CONTENT);
		}
		Collection<TareaMapper> itemsTareaMapper = MapperUtil.convertTareasMapper(itemsTarea);
		return new ResponseEntity<>(itemsTareaMapper, HttpStatus.OK);
	}
	
	@PostMapping("/registrar")
	public ResponseEntity<?> registrar(@RequestBody TareaMapper tareaMapp) {
		
		Tarea tarea = new Tarea();
		Empleado empleado = new Empleado(tareaMapp.getEmpleado_id());
		Curso curso = new Curso(tareaMapp.getCurso_id());
		Grado grado = new Grado(tareaMapp.getGrado_id());
		
		tarea.setDescripcion(tareaMapp.getDescripcion());
		tarea.setEmpleado(empleado);
		tarea.setCurso(curso);
		tarea.setGrado(grado);
		
		tareaService.insert(tarea);
		return new ResponseEntity<>("¡Se creo la tarea con éxito!", HttpStatus.CREATED);
	}
	
	@PutMapping("/editar/{tarea_id}")
	public ResponseEntity<?> editar(@RequestBody Tarea editTarea, @PathVariable Integer tarea_id) {

		Tarea tareaDB = tareaService.findById(tarea_id);

		if (tareaDB != null) {
			tareaDB.setDescripcion(editTarea.getDescripcion());
			
			return new ResponseEntity<>("¡Tarea actualizado!", HttpStatus.OK);
		}

		return new ResponseEntity<>("¡Tarea no encontrado!", HttpStatus.NOT_FOUND);
	}
	
	@DeleteMapping("/borrar/{tarea_id}")
	public ResponseEntity<?> borrar(@PathVariable Integer tarea_id) {
		Tarea tareaDB = tareaService.findById(tarea_id);

		if (tareaDB != null) {
			tareaService.delete(tarea_id);
			return new ResponseEntity<>("¡Tarea eliminado!", HttpStatus.OK);
		}
		return new ResponseEntity<>("¡Tarea no encontrado!", HttpStatus.NOT_FOUND);
	}
	
	//Servicios personalizados
	@GetMapping("/obtenerTarea/{grado_id}/{curso_id}/{fecha_registro}")
	private ResponseEntity<?> tarea(@PathVariable Integer grado_id,@PathVariable Integer curso_id,
			@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd")  Date fecha_registro){
		
		Tarea tarea = tareaService.tarea(grado_id, curso_id, fecha_registro);
		
		if(tarea == null) {
			return new ResponseEntity<>("No se ninguna tarea", HttpStatus.NO_CONTENT);
		}
		
		TareaMapper tareaMapper = MapperUtil.convertTareaMapper(tarea);
		return new ResponseEntity<>(tareaMapper, HttpStatus.OK);
	}
	
	@PutMapping("/editarTareaAlum")
	public ResponseEntity<?> editarTareaAlum(@RequestBody TareaMapper tareaMapper) {
		
		Tarea tareaDB = tareaService.tareaAlum(tareaMapper.getEmpleado_id(), tareaMapper.getCurso_id(),
					tareaMapper.getGrado_id(), tareaMapper.getDescripcion(), tareaMapper.getFecha_registro());
		
		if (tareaDB != null) {
			if(tareaMapper.getDescripcion().trim().equals("")) {
				System.out.println("Ingreso a eliminar");
				tareaService.delete(tareaDB.getTarea_id());
				return new ResponseEntity<>("¡Tarea eliminada!", HttpStatus.OK);
			}else {
				tareaDB.setDescripcion(tareaMapper.getDescripcion());
				tareaService.update(tareaDB);
				return new ResponseEntity<>("¡Tarea actualizado!", HttpStatus.OK);
			}

		}

		return new ResponseEntity<>("¡Tarea no encontrado!", HttpStatus.NOT_FOUND);
	}
	
	@GetMapping("/listaTareasGradoFecha/{alumno_id}/{fecha_registro}")
	private ResponseEntity<?> listaTareasGradoFecha(@PathVariable Integer alumno_id,
			@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Date fecha_registro) {

		Collection<Tarea> itemsTarea = tareaService.tareasGradoFecha(alumno_id, fecha_registro);
		
		if (itemsTarea.isEmpty()) {
			Mensaje mensaje = new Mensaje("¡No se encontro tareas registradas!");
			return new ResponseEntity<>(mensaje, HttpStatus.NOT_FOUND);
		}

		Collection<VerTareaMapper> itemsVerTareaMapper = MapperUtil.convertVerTareaMapper(itemsTarea);
		return new ResponseEntity<>(itemsVerTareaMapper, HttpStatus.OK);
	}
	
	
}
