package com.olivos.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.olivos.mapper.CursoMapper;
import com.olivos.model.Curso;
import com.olivos.service.CursoService;
import com.olivos.util.MapperUtil;

@RestController
@RequestMapping("/curso")
public class CursoController {
	
	@Autowired
	private CursoService cursoService;
	
	@GetMapping("/listar")
	private ResponseEntity<?> listar() {
		Collection<Curso> itemsCurso = cursoService.findAll();

		if (itemsCurso.isEmpty()) {
			return new ResponseEntity<>("No se encontro lista de cursos", HttpStatus.NO_CONTENT);
		}
		Collection<CursoMapper> itemsCursoMapper = MapperUtil.convertCursoMapper(itemsCurso);
		return new ResponseEntity<>(itemsCursoMapper, HttpStatus.OK);
	}
	
	@PostMapping("/registrar")
	public ResponseEntity<?> registrar(@RequestBody Curso curso) {

		cursoService.insert(curso);
		return new ResponseEntity<>("¡Se creo el curso con éxito!", HttpStatus.CREATED);
	}
	
	@PutMapping("/editar/{curso_id}")
	public ResponseEntity<?> editar(@RequestBody Curso editCurso, @PathVariable Integer curso_id) {

		Curso cursoDB = cursoService.findById(curso_id);

		if (cursoDB != null) {
			cursoDB.setNombre(editCurso.getNombre());
			
			return new ResponseEntity<>("¡Curso actualizado!", HttpStatus.OK);
		}

		return new ResponseEntity<>("¡Curso no encontrado!", HttpStatus.NOT_FOUND);
	}
	
	@DeleteMapping("/borrar/{curso_id}")
	public ResponseEntity<?> borrar(@PathVariable Integer curso_id) {
		Curso cursoDB = cursoService.findById(curso_id);

		if (cursoDB != null) {
			cursoService.delete(curso_id);
			return new ResponseEntity<>("¡Curso eliminado!", HttpStatus.OK);
		}
		return new ResponseEntity<>("¡Curso no encontrado!", HttpStatus.NOT_FOUND);
	}
	
	//Servicios Personalizados
	@GetMapping("/listarCursoGrado/{grado_id}")
	private ResponseEntity<?> listarCursoGrado(@PathVariable Integer grado_id) {
		Collection<Curso> itemsCurso = cursoService.listarCursoGrado(grado_id);

		if (itemsCurso.isEmpty()) {
			return new ResponseEntity<>("No se encontro lista de cursos", HttpStatus.NO_CONTENT);
		}
		Collection<CursoMapper> itemsCursoMapper = MapperUtil.convertCursoMapper(itemsCurso);
		return new ResponseEntity<>(itemsCursoMapper, HttpStatus.OK);
	}
}
